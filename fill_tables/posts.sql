INSERT INTO forum_system.posts (post_id, user_id, title, content, created_at) VALUES (2, 1, 'Help with recursion', 'I was wondering if someone could show me how to turn my code into a recursive method to
build my binary tree. I am new to recursion and it doesn''t make a whole lot of
sense to me.', '2022-10-21 00:00:00');
INSERT INTO forum_system.posts (post_id, user_id, title, content, created_at) VALUES (3, 2, 'Why is my Quick Sort using Iteration not working properly?', 'I am currently trying to benchmark a Quick Sort algorithm using both Iteration and
Recursion. My problem is with my output, my recursive function works properly and
sorts the elements. My iterative function is not working properly though. Here are 2
sample output''s and my code that I have. You will see that the iterative function
works sometimes but not every time. How do I fix this problem?', '2022-10-20 00:00:00');
INSERT INTO forum_system.posts (post_id, user_id, title, content, created_at) VALUES (4, 3, 'What''s a good resource to practice Algorithms?', 'I''m a beginner learning core JAVA. I''m looking for a resource for BEGINNERS (website, book,video, etc) to help me practice writing algorithms in plain English for a particular problem
statement, in which I would later convert into JAVA syntax? I can''t find seem to find any
good resources on the web to practice writing algorithms for problem statements to
test/improve my logical skills.
Can anyone share any resources that have problems statements w/ solutions?
This way I could compare the algorithm I come up with against the solution and get
better at writing algorithms.
Thanks in advance!', '2022-10-20 00:00:00');
INSERT INTO forum_system.posts (post_id, user_id, title, content, created_at) VALUES (5, 4, 'How to output a pyramid of stars with JOptionPane', 'Hi, here is my code so far. I was told we need to use two recursive methods. Any help will be very much appreciated. Thank you in advance.', '2022-10-21 00:00:00');
