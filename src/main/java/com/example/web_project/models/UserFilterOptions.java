package com.example.web_project.models;

import java.util.Optional;

public class UserFilterOptions {

    Optional<String> search;

    Optional<String> username;

    Optional<String> firstName;

    Optional<String> lastName;

    Optional<String> email;
    Optional<String> sortUsers;

    public UserFilterOptions() {
        this(null, null, null, null, null, null);
    }

    public UserFilterOptions(String search,
                             String username,
                             String firstName,
                             String lastName,
                             String email,
                             String sortUsers) {
        this.search = Optional.ofNullable(search);
        this.username = Optional.ofNullable(username);
        this.firstName = Optional.ofNullable(firstName);
        this.lastName = Optional.ofNullable(lastName);
        this.email = Optional.ofNullable(email);
        this.sortUsers = Optional.ofNullable(sortUsers);
    }

    public Optional<String> getSearch() {
        return search;
    }

    public void setSearch(Optional<String> search) {
        this.search = search;
    }

    public Optional<String> getSortUsers() {
        return sortUsers;
    }

    public void setSortUsers(Optional<String> sortUsers) {
        this.sortUsers = sortUsers;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(Optional<String> username) {
        this.username = username;
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public void setFirstName(Optional<String> firstName) {
        this.firstName = firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public void setLastName(Optional<String> lastName) {
        this.lastName = lastName;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public void setEmail(Optional<String> email) {
        this.email = email;
    }
}
