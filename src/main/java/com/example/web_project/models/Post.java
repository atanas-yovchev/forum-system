package com.example.web_project.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User creator;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "created_at", updatable = false)
    @CreationTimestamp
    private LocalDateTime creationDate;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "post")
    private Set<Comment> comments;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "posts_votes",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "vote_id"))
    @MapKeyJoinColumn(name = "user_id")
    private Map<User, Vote> votes;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "posts_tags",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags;

    public Post() {
    }



    public void addTags(Tag... tagsToAdd) {
        tags.addAll(Arrays.asList(tagsToAdd));
    }

    public void removeTags(Tag... tagsToRemove) {
        tags.removeAll(Arrays.asList(tagsToRemove));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Set<Comment> getComments() {
        return new TreeSet<>(comments);
    }

    public LocalDateTime getCreationDate() {

        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Map<User, Vote> getVotes() {
        return votes;
    }

    public void setVotes(Map<User, Vote> votes) {
        this.votes = votes;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return id == post.id && Objects.equals(creator, post.creator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creator, title);
    }

    public int getLikes(){
        if(votes.isEmpty())
            return 0;
        int likes = votes.values().stream()
                .filter(vote -> vote.getVoteType().equalsIgnoreCase("like")).collect(Collectors.toList())
                .size();
        int dislikes = votes.values().stream()
                .filter(vote -> vote.getVoteType().equalsIgnoreCase("dislike")).collect(Collectors.toList())
                .size();

        return likes-dislikes;
    }

    public String dateToString() {
        return creationDate.format(DateTimeFormatter.ofPattern("MMMM dd. yyyy",new Locale("en")));
    }
}