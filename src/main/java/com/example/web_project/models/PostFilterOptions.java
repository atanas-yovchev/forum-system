package com.example.web_project.models;


import java.util.Optional;

public class PostFilterOptions {

    private Optional<String> search;
    private Optional<String> username;
    private Optional<String> title;
    private Optional<String> tag;
    private Optional<String> sort;

    public PostFilterOptions() {
        this(null,null, null, null, null);
    }

    public PostFilterOptions(String search,
                             String username,
                             String title,
                             String tag,
                             String sort) {
        this.search = Optional.ofNullable(search);
        this.username = Optional.ofNullable(username);
        this.title = Optional.ofNullable(title);
        this.tag = Optional.ofNullable(tag);
        this.sort = Optional.ofNullable(sort);
    }

    public Optional<String> getSearch() {
        return search;
    }

    public void setSearch(Optional<String> search) {
        this.search = search;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(Optional<String> userName) {
        this.username = userName;
    }

    public Optional<String> getTitle() {
        return title;
    }

    public void setTitle(Optional<String> title) {
        this.title = title;
    }

    public Optional<String> getTag() {
        return tag;
    }

    public void setTag(Optional<String> tag) {
        this.tag = tag;
    }

    public Optional<String> getSort() {
        return sort;
    }

    public void setSort(Optional<String> sort) {
        this.sort = sort;
    }
}
