package com.example.web_project.models;

import org.hibernate.annotations.CreationTimestamp;


import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "comments")
public class Comment implements Comparable<Comment>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private int id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User creator;
    @Column(name = "content")
    private String content;

    @Column(name = "created_at", updatable = false)
    @CreationTimestamp
    private LocalDateTime creationDate;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "comments_votes",
            joinColumns = @JoinColumn(name = "comment_id"),
            inverseJoinColumns = @JoinColumn(name = "vote_id"))
    @MapKeyJoinColumn(name = "user_id")
    private Map<User, Vote> votes;

    @ManyToOne
    @JoinColumn(name = "post_id", updatable = false)
    private Post post;

    public Comment() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Map<User, Vote> getVotes() {
        return votes;
    }

    public void setVotes(Map<User, Vote> votes) {
        this.votes = votes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id == comment.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public int getLikes(){
        if(this.votes.isEmpty())
            return 0;
        int likes = this.votes.values().stream()
                .filter(vote -> vote.getVoteType().equalsIgnoreCase("like")).collect(Collectors.toList())
                .size();
        int dislikes = this.votes.values().stream()
                .filter(vote -> vote.getVoteType().equalsIgnoreCase("dislike")).collect(Collectors.toList())
                .size();

        return likes-dislikes;
    }

    @Override
    public int compareTo(Comment o) {
        return this.getCreationDate().compareTo(o.creationDate);
    }

    public String dateToString() {
        return creationDate.format(DateTimeFormatter.ofPattern("MMMM dd. yyyy",new Locale("en")));
    }
}
