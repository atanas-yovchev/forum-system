package com.example.web_project.models.dtos;

import java.util.Optional;

public class UserFilterDto {

    Optional<String> search;

    Optional<String> sortUser;

    public UserFilterDto(String search, String sortUser) {
        this.search = Optional.ofNullable(search);
        this.sortUser = Optional.ofNullable(sortUser);
    }

    public UserFilterDto() {
    }

    public Optional<String> getSearch() {
        return search;
    }

    public void setSearch(Optional<String> search) {
        this.search = search;
    }

    public Optional<String> getSortUser() {
        return sortUser;
    }

    public void setSortUser(Optional<String> sortUser) {
        this.sortUser = sortUser;
    }
}
