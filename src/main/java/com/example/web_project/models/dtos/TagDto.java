package com.example.web_project.models.dtos;

import org.springframework.lang.NonNull;

import javax.validation.constraints.NotNull;
import java.util.List;

public class TagDto {

    @NotNull(message = "You should add at least 1 tag")
    private List<String> tagNames;

    public List<String> getTagNames() {
        return tagNames;
    }

    public void setTagNames(List<String> tagNames) {
        this.tagNames = tagNames;
    }
}
