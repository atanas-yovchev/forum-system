package com.example.web_project.models.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.time.LocalDateTime;
import java.util.Set;

public class UserDtoOut {

    private String firstName;
    private String lastName;

    @JsonInclude(Include.NON_NULL)
    private String phoneNumber;
    private String email;
    private String username;
    private String password;
    private boolean isAdmin;
    private boolean isBlocked;
    private Set<PostDtoOut> posts;
    private Set<CommentDtoOut> comments;
    private LocalDateTime creationDate;
    private String avatarPath;


    public UserDtoOut() {
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public Set<PostDtoOut> getPosts() {
        return posts;
    }

    public void setPosts(Set<PostDtoOut> posts) {
        this.posts = posts;
    }

    public Set<CommentDtoOut> getComments() {
        return comments;
    }

    public void setComments(Set<CommentDtoOut> comments) {
        this.comments = comments;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phone) {
        this.phoneNumber = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }
}


