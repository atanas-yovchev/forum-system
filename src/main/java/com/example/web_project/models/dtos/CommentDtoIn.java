package com.example.web_project.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CommentDtoIn {
    @NotNull
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
