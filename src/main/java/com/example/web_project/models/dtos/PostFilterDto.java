package com.example.web_project.models.dtos;

import java.util.Optional;

public class PostFilterDto {

    Optional<String> search;
    Optional<String> sortPost;

    public PostFilterDto(String search, String sortPost) {
        this.search = Optional.ofNullable(search);
        this.sortPost = Optional.ofNullable(sortPost);
    }

    public PostFilterDto() {
    }

    public Optional<String> getSearch() {
        return search;
    }

    public void setSearch(Optional<String> search) {
        this.search = search;
    }

    public Optional<String> getSortPost() {
        return sortPost;
    }

    public void setSortPost(Optional<String> sortPost) {
        this.sortPost = sortPost;
    }
}
