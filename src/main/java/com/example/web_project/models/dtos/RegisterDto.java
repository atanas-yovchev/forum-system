package com.example.web_project.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegisterDto {
    @NotNull(message = "First name can't be empty")
    @Size(min = 4, max = 32, message = "First name should be between 4 and 32 symbols")
    private String firstName;

    @NotNull(message = "Last name can't be empty")
    @Size(min = 4, max = 32, message = "Last name should be between 4 and 32 symbols")
    private String lastName;

    @Email(message = "Email is not valid", regexp = "([a-z0-9.]+)[@]([a-z]+)[.]([a-z]+)")
    @NotNull(message = "Email cannot be empty")
    private String email;

    @NotNull(message = "Username can't be empty")
    @Size(min = 4, max = 32, message = "Username should be between 4 and 32 symbols")
    private String username;
    @NotNull(message = "Password can't be empty")
    @Size(min = 8, max = 32, message = "Password should be between 8 and 32 symbols")
    private String password;
    @NotNull(message = "Password can't be empty")
    @Size(min = 8, max = 32, message = "Password should be between 8 and 32 symbols")
    private String passwordConfirm;

    public RegisterDto() {
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public RegisterDto(String firstName, String lastName, String email, String username, String password) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}


