package com.example.web_project.utils.mappers;

import com.example.web_project.models.Post;
import com.example.web_project.models.Tag;
import com.example.web_project.models.User;
import com.example.web_project.models.dtos.PostDtoEdit;
import com.example.web_project.models.dtos.PostDtoIn;
import com.example.web_project.models.dtos.PostDtoOut;
import com.example.web_project.services.PostService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.Collectors;

@Component
public class PostMapper {

    private final PostService postService;

    public PostMapper(PostService postService) {

        this.postService = postService;
    }

    public Post fromDtoUpdate(int id, PostDtoIn postDtoIn) {
        Post post = postService.getPostById(id);
        post.setContent(postDtoIn.getContent());
        post.setTitle(postDtoIn.getTitle());
        return post;
    }

    public Post fromDtoUpdate(int id, PostDtoEdit postDtoEdit) {
        Post post = postService.getPostById(id);
        post.setContent(postDtoEdit.getContent());
        post.setTitle(postDtoEdit.getTitle());
        return post;
    }

    public Post fromDtoCreate(PostDtoIn postDtoIn, User loggedUser) {
        Post post = new Post();

        post.setTitle(postDtoIn.getTitle());
        post.setContent(postDtoIn.getContent());
        post.setCreator(loggedUser);
        post.setComments(new HashSet<>());
        post.setVotes(new HashMap<>());
        post.setTags(new HashSet<>());
        return post;
    }

    public Post fromDtoCreate(PostDtoEdit postDtoEdit, User loggedUser) {
        Post post = new Post();

        post.setTitle(postDtoEdit.getTitle());
        post.setContent(postDtoEdit.getContent());
        post.setCreator(loggedUser);
        post.setComments(new HashSet<>());
        post.setVotes(new HashMap<>());
        post.setTags(new HashSet<>());
        return post;
    }

    public PostDtoOut toDto(Post post) {
        PostDtoOut postDtoOut = new PostDtoOut();

        postDtoOut.setTitle(post.getTitle());
        postDtoOut.setContent(post.getContent());
        postDtoOut.setCreator(post.getCreator());
        postDtoOut.setComments(post.getComments());
        postDtoOut.setVotes(post.getVotes().entrySet().stream()
                .map(entry -> String.format("%s : %s", entry.getKey().getUsername(),
                        entry.getValue().getVoteType()))
                .collect(Collectors.toList()));
        postDtoOut.setCreationDate(post.getCreationDate());
        postDtoOut.setTags(post.getTags());

        return postDtoOut;
    }

    public PostDtoEdit editExisting(Post post) {
        PostDtoEdit postDtoEdit = new PostDtoEdit();
        postDtoEdit.setContent(post.getContent());
        postDtoEdit.setTitle(post.getTitle());
        postDtoEdit.setId(post.getId());

        return postDtoEdit;
    }
}
