package com.example.web_project.utils.mappers;

import com.example.web_project.models.Phone;
import com.example.web_project.models.User;
import com.example.web_project.models.dtos.RegisterDto;
import com.example.web_project.models.dtos.UpdateUserDto;
import com.example.web_project.models.dtos.UserDtoOut;
import com.example.web_project.services.PhoneService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.Objects;

;

@Component
public class UserMapper {
    private static final String DEFAULT_AVATAR = "/assets/avatars/default/OIP.jpeg";
    private static final String INVALID_AVATAR = "The photo you selected is not appropriate";
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;
    private final PhoneService phoneService;

    public UserMapper(PostMapper postMapper, CommentMapper commentMapper, PhoneService phoneService) {
        this.postMapper = postMapper;
        this.commentMapper = commentMapper;
        this.phoneService = phoneService;
    }

    public User fromDtoUpdate(User user, UpdateUserDto userDtoInUpdate) {
        user.setFirstName(userDtoInUpdate.getFirstName());
        user.setLastName(userDtoInUpdate.getLastName());
        user.setEmail(userDtoInUpdate.getEmail());

        if (userDtoInUpdate.getPhone() != null && !userDtoInUpdate.getPhone().isEmpty()) {
            Phone phone = new Phone();
            phone.setUser(user);
            phone.setPhoneNumber(userDtoInUpdate.getPhone());
            if (user.getPhone() == null) {
                phoneService.create(phone, user);
                user.setPhone(phone);
            } else if(!userDtoInUpdate.getPhone().equals(user.getPhone().getPhoneNumber())) {
                phoneService.update(phone, user);
            }
        }

        return user;
    }

    public User fromDtoUpdate(User user, UpdateUserDto userDtoInUpdate, MultipartFile multipartFile) {

        User updated = fromDtoUpdate(user, userDtoInUpdate);
        if (!multipartFile.isEmpty()) {
            changeAvatar(user, multipartFile);
        }
        return updated;
    }

    public User fromDtoCreate(RegisterDto registerDto) {
        User user = new User();

        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setEmail(registerDto.getEmail());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setAvatarPath(DEFAULT_AVATAR);
        //Is this needed?
        user.setRoles(new HashSet<>());
        return user;
    }

    public UserDtoOut toDto(User user) {
        UserDtoOut userDtoOut = new UserDtoOut();
        userDtoOut.setUsername(user.getUsername());
        userDtoOut.setPassword(user.getPassword());
        userDtoOut.setEmail(user.getEmail());
        userDtoOut.setFirstName(user.getFirstName());
        userDtoOut.setLastName(user.getLastName());
        userDtoOut.setCreationDate(user.getCreatedDate());
        userDtoOut.setAvatarPath(user.getAvatarPath());
        if (user.getPhone() != null) {
            userDtoOut.setPhoneNumber(user.getPhone().getPhoneNumber());
        }
        userDtoOut.setAdmin(user.isAdmin());
        userDtoOut.setBlocked(user.isBlocked());
        return userDtoOut;
    }

    public UpdateUserDto toUpdateDto(User user) {
        UpdateUserDto updateUserDto = new UpdateUserDto();

        updateUserDto.setAdmin(user.isAdmin());
        updateUserDto.setFirstName(user.getFirstName());
        updateUserDto.setLastName(user.getLastName());
        updateUserDto.setEmail(user.getEmail());
        updateUserDto.setAvatarPath(user.getAvatarPath());
        if (user.getPhone() != null)
            updateUserDto.setPhone(user.getPhone().getPhoneNumber());
        else updateUserDto.setPhone("");
        return updateUserDto;
    }

    private void changeAvatar(User user, MultipartFile multipartFile) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(multipartFile.getOriginalFilename()));
        String uploadDir = "src/main/resources/static/assets/avatars/" + user.getUsername();
        try {
            saveFile(uploadDir, fileName, multipartFile);
        } catch (IOException e) {
            throw new UnsupportedOperationException(INVALID_AVATAR);
        }
        user.setAvatarPath("/assets/avatars/" + user.getUsername() + "/" + fileName);
    }

    public static void saveFile(String uploadDir, String fileName,
                                MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {
            throw new IOException("Could not save image file: " + fileName, ioe);
        }
    }
}
