package com.example.web_project.utils.mappers;

import com.example.web_project.models.User;
import com.example.web_project.models.Phone;
import com.example.web_project.services.PhoneService;
import com.example.web_project.services.UserService;
import org.springframework.stereotype.Component;

@Component
public class PhoneMapper {

    private final PhoneService phoneService;
    private final UserService userService;

    public PhoneMapper(PhoneService phoneService, UserService userService) {
        this.phoneService = phoneService;
        this.userService = userService;
    }

    public Phone fromStringToCreate(int userId, String phoneNumber) {
        Phone phone = new Phone();
        phone.setPhoneNumber(phoneNumber);
        phone.setUser(userService.getUserById(userId));
        return phone;
    }

    public Phone fromStringToUpdate(int userId, User loggedUser, String phoneNumber) {
        Phone phone = getExistingFromId(userId, loggedUser);
        phone.setPhoneNumber(phoneNumber);
        return phone;
    }

    public Phone getExistingFromId(int userId, User loggedUser) {
        return phoneService.getPhoneByUser(userService.getUserById(userId), loggedUser);
    }
}
