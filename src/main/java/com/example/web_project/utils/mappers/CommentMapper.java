package com.example.web_project.utils.mappers;

import com.example.web_project.models.Comment;
import com.example.web_project.models.Post;
import com.example.web_project.models.User;
import com.example.web_project.models.dtos.CommentDtoEdit;
import com.example.web_project.models.dtos.CommentDtoIn;
import com.example.web_project.models.dtos.CommentDtoOut;
import com.example.web_project.services.CommentService;
import com.example.web_project.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.stream.Collectors;

@Component
public class CommentMapper {
    private final CommentService commentService;
    private final PostService postService;

    @Autowired
    public CommentMapper(CommentService commentService, PostService postService) {
        this.commentService = commentService;
        this.postService = postService;
    }

    public Comment fromDtoUpdate(int id, CommentDtoIn commentDtoIn) {
        Comment comment = commentService.getCommentById(id);
        comment.setContent(commentDtoIn.getContent());
        return comment;
    }

    public Comment fromDtoCreate(int postId, CommentDtoIn commentDtoIn, User creator) {
        Post post = postService.getPostById(postId);
        Comment comment = new Comment();
        comment.setContent(commentDtoIn.getContent());
        comment.setCreator(creator);
        comment.setPost(post);
        comment.setVotes(new HashMap<>());
        return comment;
    }

    public CommentDtoOut toDto(Comment comment) {
        CommentDtoOut commentDtoOut = new CommentDtoOut();
        commentDtoOut.setContent(comment.getContent());
        commentDtoOut.setCreator(comment.getCreator());
        commentDtoOut.setCreationDate(comment.getCreationDate());
        commentDtoOut.setPost(comment.getPost());
        commentDtoOut.setVotes(comment.getVotes().entrySet().stream()
                .map(entry -> String.format("%s : %s", entry.getKey().getUsername(),
                        entry.getValue().getVoteType()))
                .collect(Collectors.toList()));

        return commentDtoOut;
    }

    public CommentDtoEdit toEditDto(Comment comment) {
        CommentDtoEdit dto = new CommentDtoEdit();
        dto.setId(comment.getId());
        dto.setContent(comment.getContent());
        return dto;
    }
}
