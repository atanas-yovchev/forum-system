package com.example.web_project.utils.mappers;

import com.example.web_project.models.Tag;
import com.example.web_project.services.UserService;
import org.springframework.stereotype.Component;

@Component
public class TagMapper {

    public Tag fromTagName(String tagName) {
        Tag tag = new Tag();
        tag.setName(tagName);
        return tag;
    }

}
