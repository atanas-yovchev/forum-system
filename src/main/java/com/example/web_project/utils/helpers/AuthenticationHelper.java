package com.example.web_project.utils.helpers;

import com.example.web_project.models.User;
import com.example.web_project.models.dtos.LoginDto;
import com.example.web_project.services.UserService;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String INVALID_CREDENTIALS_ERROR = "Invalid username or password.";
    public static final String NOT_AUTHENTICATED_ERROR = "Access to this resource requires authentication.";
    private final UserService service;

    @Autowired
    public AuthenticationHelper(UserService service) {
        this.service = service;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    NOT_AUTHENTICATED_ERROR);
        }

        String userInfo = headers.getFirst(AUTHORIZATION_HEADER_NAME);
        String username = getUserName(userInfo);
        String password = getPassword(userInfo);

        try {
            User user = service.getUserByUsername(username);

            if (!user.getPassword().equals(password)) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                        INVALID_CREDENTIALS_ERROR);
            }

            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_CREDENTIALS_ERROR);
        }
    }


    private String getUserName(String userInfo) {
        int firstSpace = userInfo.indexOf(" ");
        if (firstSpace == -1) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    INVALID_CREDENTIALS_ERROR);
        }
        return userInfo.substring(0, firstSpace);
    }

    private String getPassword(String userInfo) {
        int firstSpace = userInfo.indexOf(" ");
        if (firstSpace == -1) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_CREDENTIALS_ERROR);
        }

        return userInfo.substring(firstSpace + 1);
    }

    public User tryGetUser(LoginDto login) {
        try {
            User user = service.getUserByUsername(login.getUsername());
            if (!user.getPassword().equals(login.getPassword())) {
                throw new UnauthorizedOperationException(INVALID_CREDENTIALS_ERROR);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException(INVALID_CREDENTIALS_ERROR);
        }
    }
}
