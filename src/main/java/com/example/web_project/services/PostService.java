package com.example.web_project.services;

import com.example.web_project.models.Post;
import com.example.web_project.models.PostFilterOptions;
import com.example.web_project.models.Tag;
import com.example.web_project.models.User;
import com.example.web_project.models.dtos.PostFilterDto;

import java.util.List;

public interface PostService {
    List<Post> getAll(PostFilterOptions postFilterOptions);

    List<Post> getTenMostCommentedPosts();

    List<Post> getTenLastCreatedPosts();
    Post getPostById(int id);

    Post create(Post post);

    Post update(Post post, User user);

    Post delete(int id, User user);

    void addVote(User user, Post post, int voteId);

    void removeVote(User user, Post post);

    void removeTag(User loggedUser, Post post, int tagId);

    String deleteTag(User loggedUser, int tagId);

    void addTags(User loggedUser, Post post, List<Tag> tags);

    List<Tag> getAllTags();
    List<Post> filter(PostFilterDto postFilterDto);
}
