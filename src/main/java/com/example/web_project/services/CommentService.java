package com.example.web_project.services;

import com.example.web_project.models.Comment;
import com.example.web_project.models.User;

import java.util.List;
import java.util.Optional;

public interface CommentService {

    List<Comment> getAll(Optional<String> username,
                         Optional<String> content,
                         Optional<Integer> postId,
                         Optional<String> sort);

    Comment getCommentById(int commentId);

    Comment create(Comment comment);

    Comment update(Comment comment, User user);

    Comment delete(int id, User user);

    void addVote(Comment comment, User user, int voteId);

    void removeVote(Comment comment, User user);
}
