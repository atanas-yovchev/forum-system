package com.example.web_project.services;

import com.example.web_project.models.User;
import com.example.web_project.models.Phone;

public interface PhoneService {

    Phone create(Phone phone, User loggedUser);

    Phone update(Phone phone, User loggedUser);

    Phone delete(Phone phone, User loggedUser);

    Phone getPhoneByUser(User user, User loggedUser);


}
