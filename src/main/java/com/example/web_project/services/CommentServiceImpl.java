package com.example.web_project.services;

import com.example.web_project.models.Comment;
import com.example.web_project.models.User;
import com.example.web_project.models.Vote;
import com.example.web_project.repositories.CommentRepository;
import com.example.web_project.repositories.VoteRepository;
import com.example.web_project.utils.exceptions.BlockedUserException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {


    private static final String NOT_AUTHORIZED_ERROR = "You're not authorized to modify this user.";
    private final CommentRepository commentRepository;
    private final VoteRepository voteRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository,
                              VoteRepository voteRepository) {
        this.commentRepository = commentRepository;
        this.voteRepository = voteRepository;
    }


    @Override
    public List<Comment> getAll(Optional<String> username,
                                Optional<String> content,
                                Optional<Integer> postId,
                                Optional<String> sort) {
        return commentRepository.getAll(username, content, postId, sort);
    }

    @Override
    public Comment getCommentById(int commentId) {
        return commentRepository.getCommentById(commentId);
    }

    @Override
    public Comment create(Comment comment) {

        if (comment.getCreator().isBlocked()) {
            throw new BlockedUserException("Blocked users cannot create comments");
        }

        return commentRepository.create(comment);
    }

    @Override
    public Comment update(Comment comment, User user) {

        if (comment.getCreator().isBlocked() && comment.getCreator().equals(user)) {
            throw new BlockedUserException("Blocked users cannot update comments");
        }

        checkModifyPermissions(comment.getId(), user);
        return commentRepository.update(comment);
    }

    @Override
    public Comment delete(int id, User user) {
        checkModifyPermissions(id, user);
        return commentRepository.delete(id);
    }

    @Override
    public void addVote(Comment comment, User user, int voteId) {
        Vote vote = voteRepository.getById(voteId);
        comment.getVotes().put(user, vote);
        commentRepository.update(comment);
    }

    @Override
    public void removeVote(Comment comment, User user) {
        comment.getVotes().remove(user);
        commentRepository.update(comment);
    }

    private void checkModifyPermissions(int id, User user) {
        Comment commentToModify = commentRepository.getCommentById(id);
        if (!commentToModify.getCreator().equals(user) && !user.isAdmin()) {
            throw new UnauthorizedOperationException(NOT_AUTHORIZED_ERROR);
        }
    }
}
