package com.example.web_project.services;

import com.example.web_project.models.User;
import com.example.web_project.models.UserFilterOptions;
import com.example.web_project.models.dtos.UserFilterDto;
import com.example.web_project.repositories.PhoneRepository;
import com.example.web_project.repositories.RoleRepository;
import com.example.web_project.repositories.UserRepository;
import com.example.web_project.utils.exceptions.DuplicateEntityException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.SoftDeletedEntityException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final String NOT_AUTHORIZED_ERROR = "You're not authorized to modify this user.";
    private final UserRepository repository;
    private final RoleRepository roleRepository;
    private final PhoneRepository phoneRepository;


    @Autowired
    public UserServiceImpl(UserRepository repository, RoleRepository roleRepository, PhoneRepository phoneRepository) {
        this.repository = repository;
        this.roleRepository = roleRepository;
        this.phoneRepository = phoneRepository;
    }

    @Override
    public List<User> getAll(UserFilterOptions userFilterOptions) {
        return repository.getAll(userFilterOptions);
    }

    @Override
    public User getUserById(int id) {
        return repository.getUserById(id);
    }

    @Override
    public User getUserByUsername(String username) {
        return repository.getUserByUsername(username);
    }


    @Override
    public User create(User user) {
        if (repository.isUserSoftDeleted(user.getEmail(), user.getUsername())) {
            throw new SoftDeletedEntityException("User", "username", "email", user.getUsername(), user.getEmail());
        }
        if (duplicateUsername(user)) throw new DuplicateEntityException("User", "username", user.getUsername());
        if (duplicateEmail(user)) throw new DuplicateEntityException("User", "email", user.getEmail());

        return repository.create(user);
    }

    @Override
    public User update(User updatedUser, User authenticatedUser) {
        checkModifyPermissions(updatedUser, authenticatedUser);
        if (duplicateUsername(updatedUser)) {
            throw new DuplicateEntityException("User", "username", updatedUser.getUsername());
        }
        if (duplicateEmail(updatedUser)) {
            throw new DuplicateEntityException("User", "email", updatedUser.getEmail());
        }
        return repository.update(updatedUser);
    }

    @Override
    public User addAdminRole(User authenticatedUser, int id) {
        User userToPromote = repository.getUserById(id);
        if (!authenticatedUser.isAdmin()) {
            throw new UnauthorizedOperationException(NOT_AUTHORIZED_ERROR);
        }
        if (userToPromote.isAdmin()) {
            throw new DuplicateEntityException(String.format("User with id %d is already admin.", id));
        }

        userToPromote.addRole(roleRepository.getRoleType("admin"));
        return repository.update(userToPromote);
    }

    @Override
    public User removeAdminRole(User authenticatedUser, int id) {
        User userToDemote = repository.getUserById(id);
        if (!authenticatedUser.isAdmin()) {
            throw new UnauthorizedOperationException(NOT_AUTHORIZED_ERROR);
        }
        if (!userToDemote.isAdmin()) {
            throw new DuplicateEntityException(String.format("User with id %d is not an admin.", id));
        }
        //only admins can have phones
        if (userToDemote.getPhone() != null)
            phoneRepository.delete(userToDemote.getPhone());
        userToDemote.removeRole(roleRepository.getRoleType("admin"));
        return repository.update(userToDemote);
    }

    @Override
    public User delete(int id, User authenticatedUser) {
        User userToDelete = repository.getUserById(id);
        checkModifyPermissions(userToDelete, authenticatedUser);
        return repository.delete(userToDelete);
    }

    @Override
    public User changeBlockedStatus(User authenticatedUser, int id, String status) {
        User user = getUserById(id);
        if (!authenticatedUser.isAdmin()) {
            throw new UnauthorizedOperationException(NOT_AUTHORIZED_ERROR);
        }
        if (status.equalsIgnoreCase("yes")) {
            if (user.isBlocked())
                throw new DuplicateEntityException(
                        String.format("User with id %d is already blocked", user.getId()));

        } else if (status.equalsIgnoreCase("no")) {
            if (!user.isBlocked())
                throw new DuplicateEntityException(
                        String.format("User with id %d is already unblocked", user.getId()));
        } else {
            throw new UnsupportedOperationException("Blocked status can be only yes or no");
        }

        user.setBlocked(!user.isBlocked());
        repository.update(user);
        return user;
    }

    private boolean duplicateUsername(User user) {
        boolean duplicateExist = true;
        try {
            User existingUser = repository.getUserByUsername(user.getUsername());
            if (existingUser.equals(user))
                duplicateExist = false;
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }
        return duplicateExist;
    }

    private boolean duplicateEmail(User user) {
        boolean duplicateExist = true;
        try {
            User existingUser = repository.getUserByEmail(user.getEmail());
            if (existingUser.equals(user))
                duplicateExist = false;
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }
        return duplicateExist;
    }

    @Override
    public List<User> filter(UserFilterDto userFilterDto) {
        return repository.filter(userFilterDto);
    }

    private void checkModifyPermissions(User userToModify, User authenticatedUser) {
        if (!userToModify.equals(authenticatedUser) && !authenticatedUser.isAdmin()) {
            throw new UnauthorizedOperationException(NOT_AUTHORIZED_ERROR);
        }
    }
}
