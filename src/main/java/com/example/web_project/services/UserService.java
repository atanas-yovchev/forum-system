package com.example.web_project.services;

import com.example.web_project.models.User;
import com.example.web_project.models.UserFilterOptions;
import com.example.web_project.models.dtos.UserFilterDto;

import java.util.List;

public interface UserService {

    List<User> getAll(UserFilterOptions userFilterOptions);

    User getUserById(int id);

    User getUserByUsername(String username);

    User create(User user);

    User update(User userToUpdate, User authenticatedUser);

    User delete(int id, User authenticatedUser);

    User addAdminRole(User authenticatedUser, int id);

    User removeAdminRole(User authenticatedUser, int id);

    User changeBlockedStatus(User authenticatedUser, int id, String status);

    List<User> filter(UserFilterDto userFilterDto);
}
