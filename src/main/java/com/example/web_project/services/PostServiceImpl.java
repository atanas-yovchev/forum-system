package com.example.web_project.services;

import com.example.web_project.models.*;
import com.example.web_project.models.dtos.PostFilterDto;
import com.example.web_project.repositories.PostRepository;
import com.example.web_project.repositories.TagRepository;
import com.example.web_project.repositories.VoteRepository;
import com.example.web_project.utils.exceptions.BlockedUserException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    private static final String NOT_AUTHORIZED_ERROR = "You are not authorized to modify this post.";

    public static final String NOT_AN_ADMIN_ERROR = "You're not an admin and are not authorized to perform" +
            "this operation.";
    private final PostRepository postRepository;
    private final VoteRepository voteRepository;

    private final CommentService commentService;

    private final TagRepository tagRepository;


    @Autowired
    public PostServiceImpl(PostRepository repository, VoteRepository voteRepository, CommentService commentService, TagRepository tagRepository) {
        this.postRepository = repository;
        this.voteRepository = voteRepository;
        this.commentService = commentService;
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Post> getAll(PostFilterOptions postFilterOptions) {
        return postRepository.getAll(postFilterOptions);
    }

    @Override
    public Post getPostById(int id) {
        return postRepository.getPostById(id);
    }

    @Override
    public List<Post> getTenMostCommentedPosts() {
        return postRepository.getTenMostCommentedPosts();
    }

    @Override
    public List<Post> getTenLastCreatedPosts() {
        return postRepository.getTenLastCreatedPosts();
    }

    @Override
    public Post create(Post post) {

        if (post.getCreator().isBlocked()) {
            throw new BlockedUserException("Blocked users cannot create posts");
        }

        return postRepository.create(post);
    }

    @Override
    public Post update(Post post, User user) {

        if (post.getCreator().isBlocked() && post.getCreator().equals(user)) {
            throw new BlockedUserException("Blocked users cannot update posts");
        }

        checkModifyPermissions(post, user);
        return postRepository.update(post);
    }

    @Override
    public Post delete(int id, User user) {
        Post postToDelete = postRepository.getPostById(id);
        checkModifyPermissions(postToDelete, user);
        postToDelete.getComments().forEach(comment -> commentService.delete(comment.getId(),user));
        return postRepository.delete(postToDelete);
    }

    @Override
    public void addVote(User user, Post post, int voteId) {
        Vote vote = voteRepository.getById(voteId);
        post.getVotes().put(user, vote);
        postRepository.update(post);
    }

    @Override
    public void removeVote(User user, Post post) {
        post.getVotes().remove(user);
        postRepository.update(post);
    }

    @Override
    public void addTags(User loggedUser, Post post, List<Tag> tags) {
        checkModifyPermissions(post, loggedUser);
        for (Tag tag : tags) {
            try {
                tag = tagRepository.getByName(tag.getName());

            } catch (EntityNotFoundException e) {
                tagRepository.create(tag);
            }
            if (!post.getTags().contains(tag))
                post.addTags(tag);
        }
        postRepository.update(post);
    }

    @Override
    public List<Tag> getAllTags() {
        return postRepository.getAllTags();
    }

    @Override
    public void removeTag(User loggedUser, Post post, int tagId) {
        checkModifyPermissions(post, loggedUser);
        Tag tag = tagRepository.getById(tagId);
        if (!post.getTags().contains(tag)) {
            throw new EntityNotFoundException(String.format(
                    "Post with id %d doesn't have a tag '%s'", post.getId(), tag.getName()
            ));
        }
        post.removeTags(tag);
        postRepository.update(post);
    }

    @Override
    public String deleteTag(User loggedUser, int tagId) {
        if (!loggedUser.isAdmin()) {
            throw new UnauthorizedOperationException(NOT_AN_ADMIN_ERROR);
        }
        Tag tag = tagRepository.getById(tagId);
        List<Post> postsWithTag = postRepository.getAllByTagName(tag.getName());
        postsWithTag.forEach(aPost ->
                aPost.removeTags(tag)
        );
        postsWithTag.forEach(postRepository::update);
        tagRepository.delete(tag);
        return String.format("Tag '%s' was successfully deleted and removed from all posts.", tag.getName());
    }

    @Override
    public List<Post> filter(PostFilterDto postFilterDto) {
        return postRepository.filter(postFilterDto);
    }

    private void checkModifyPermissions(Post post, User user) {
        if (!post.getCreator().equals(user) && !user.isAdmin()) {
            throw new UnauthorizedOperationException(NOT_AUTHORIZED_ERROR);
        }
    }
}
