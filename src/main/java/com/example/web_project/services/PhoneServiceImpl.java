package com.example.web_project.services;

import com.example.web_project.models.User;
import com.example.web_project.models.Phone;
import com.example.web_project.repositories.PhoneRepository;
import com.example.web_project.utils.exceptions.DuplicateEntityException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import org.springframework.stereotype.Service;

@Service
public class PhoneServiceImpl implements PhoneService{

    public static final String NOT_AN_ADMIN_ERROR = "You're not an admin and cannot have a phone number";
    public static final String NOT_YOUR_PHONE_NUMBER_ERROR = "You can only access your own phone number";
    private final PhoneRepository phoneRepository;

    public PhoneServiceImpl(PhoneRepository phoneRepository) {
        this.phoneRepository = phoneRepository;
    }

    @Override
    public Phone create(Phone phone, User loggedUser) {
        User user = phone.getUser();
        checkModifyPermissions(loggedUser, user);

        if(userHasPhone(user)) {
            throw new DuplicateEntityException("You already have a phone. If you want to change it, send a PUT query.");
        }
        if(phoneExists(phone)) {
            throw new DuplicateEntityException("Phone", "digits", phone.getPhoneNumber());
        }

        return phoneRepository.create(phone);
    }



    @Override
    public Phone update(Phone phone, User loggedUser) {
        checkModifyPermissions(loggedUser,phone.getUser());
        if(phoneExists(phone)) {
            throw new DuplicateEntityException("Phone", "digits", phone.getPhoneNumber());
        }
        return phoneRepository.update(phone);
    }

    @Override
    public Phone delete(Phone phone, User loggedUser) {
        checkModifyPermissions(loggedUser, phone.getUser());
        return phoneRepository.delete(phone);
    }

    @Override
    public Phone getPhoneByUser(User user, User loggedUser) {
        checkModifyPermissions(loggedUser, user);
        return phoneRepository.getPhoneByUser(user);
    }

    private boolean userHasPhone(User user) {
        boolean hasPhone = true;
        try {
            phoneRepository.getPhoneByUser(user);
        } catch (EntityNotFoundException e) {
            hasPhone = false;
        }

        return hasPhone;
    }


    private boolean phoneExists(Phone phone) {
        boolean exists = true;
        try {
            phoneRepository.getPhoneByNumber(phone.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            exists = false;
        }

        return exists;
    }

    private void checkModifyPermissions(User loggedUser, User user) {
        if(!loggedUser.equals(user)) {
            throw new UnauthorizedOperationException(NOT_YOUR_PHONE_NUMBER_ERROR);
        }
        if(!loggedUser.isAdmin()) {
            throw new UnauthorizedOperationException(NOT_AN_ADMIN_ERROR);
        }

    }

}
