package com.example.web_project.controllers.mvc;

import com.example.web_project.models.User;
import com.example.web_project.models.dtos.LoginDto;
import com.example.web_project.models.dtos.RegisterDto;
import com.example.web_project.services.UserService;
import com.example.web_project.utils.exceptions.DuplicateEntityException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import com.example.web_project.utils.helpers.AuthenticationHelper;
import com.example.web_project.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class AuthenticationMvcController {
    private final UserService userService;
    private final AuthenticationHelper helper;
    private final UserMapper mapper;

    @Autowired
    public AuthenticationMvcController(UserService userService, AuthenticationHelper helper, UserMapper mapper) {
        this.userService = userService;
        this.helper = helper;
        this.mapper = mapper;
    }

    @GetMapping("/login")
    private String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "Login";
    }

    @PostMapping("/login")
    private String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                               HttpSession session,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "Login";
        }

        try {
            User user = helper.tryGetUser(login);
            session.setAttribute("user", user);
            return "redirect:";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "Login";
        }catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @GetMapping("/logout")
    private String handleLogout(HttpSession session) {
        session.removeAttribute("user");
        return "redirect:";
    }

    @GetMapping("/register")
    private String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "Register";
    }

    @PostMapping("/register")
    private String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                  BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "Register";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error",
                    "Password confirmation should match password.");
            return "Register";
        }
        try {
            User user = mapper.fromDtoCreate(register);
            userService.create(user);
            return "redirect:/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "Register";
        }
    }
}
