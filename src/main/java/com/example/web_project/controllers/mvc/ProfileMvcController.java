package com.example.web_project.controllers.mvc;

import com.example.web_project.models.User;
import com.example.web_project.models.dtos.UpdateUserDto;
import com.example.web_project.models.dtos.UserDtoOut;
import com.example.web_project.services.UserService;
import com.example.web_project.utils.exceptions.DuplicateEntityException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import com.example.web_project.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/profile")
public class ProfileMvcController {
    private final UserService userService;
    private final UserMapper mapper;

    @Autowired
    public ProfileMvcController(UserService userService,
                                UserMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @ModelAttribute("isAuth")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("user") != null;
    }


    @GetMapping("/{username}")
    public String showProfile(HttpSession session, @PathVariable String username,
                              Model model) {
//        if (!populateIsAuthenticated(session)) {
//            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You must be logged in to access this resource");
//        }
        try {
            User userProfile = userService.getUserByUsername(username);
            model.addAttribute("profile", userProfile);
            return "Profile";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You're not allowed to use this resource");
        }
    }

    @GetMapping("/{username}/edit")
    public String editProfile(@PathVariable String username,
                              HttpSession session,
                              Model model) {
        User authenticated = (User) session.getAttribute("user");
        if (!authenticated.getUsername().equals(username)) {
            return "redirect:/profile/" + username;
        }
        try {
            User user = userService.getUserByUsername(username);
            UpdateUserDto updateUserDto = mapper.toUpdateDto(user);
            model.addAttribute("update", updateUserDto);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "You're not allowed to use this resource");
        }
        return "EditProfile";
    }

    @PostMapping("/{username}/edit")
    public String updateProfile(HttpSession session,
                                @RequestParam("image") MultipartFile multipartFile,
                                @Valid @ModelAttribute("update") UpdateUserDto updateUserDto,
                                BindingResult bindingResult,
                                @PathVariable String username) {
        User authenticateUser = (User) session.getAttribute("user");
        if (bindingResult.hasErrors()) {
            return "EditProfile";
        }
        try {
            User profileUser = userService.getUserByUsername(username);
            User updated = mapper.fromDtoUpdate(profileUser, updateUserDto, multipartFile);
            userService.update(updated, authenticateUser);

        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("email", "unauthorized", e.getMessage());
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "duplicate-entity", e.getMessage());
        } catch (UnsupportedOperationException e) {
            bindingResult.rejectValue("email", "fail-photo", e.getMessage());
        }
        return "redirect:/profile/" + authenticateUser.getUsername();
    }

    @PostMapping("/{username}/promote")
    public String promote(HttpSession session, @ModelAttribute("profile") User user) {
        User authenticated = (User) session.getAttribute("user");
        User profile = userService.getUserByUsername(user.getUsername());
        try {
            profile = userService.addAdminRole(authenticated, profile.getId());
            if (authenticated.equals(profile)) {
                session.setAttribute("user", profile);
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "You're not allowed to use this resource");
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You're not allowed to use this resource");
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "You're not allowed to use this resource");
        }
        return "redirect:/profile/" + profile.getUsername();
    }

    @PostMapping("/{username}/demote")
    public String demote(HttpSession session, @ModelAttribute("profile") UserDtoOut userDtoOut) {
        User authenticated = (User) session.getAttribute("user");
        User profile = userService.getUserByUsername(userDtoOut.getUsername());
        profile = userService.removeAdminRole(authenticated, profile.getId());

        if (authenticated.equals(profile)) {
            session.setAttribute("user", profile);
        }
        return "redirect:/profile/" + profile.getUsername();
    }

    @PostMapping("/{username}/blocked")
    public String blocked(HttpSession session,
                          @ModelAttribute("profile") UserDtoOut userDtoOut) {
        User authenticated = (User) session.getAttribute("user");
        User profile = userService.getUserByUsername(userDtoOut.getUsername());
        String status = profile.isBlocked() ? "no" : "yes";
        userService.changeBlockedStatus(authenticated, profile.getId(), status);
        return "redirect:/profile/" + profile.getUsername();
    }

    @PostMapping("/{username}/delete")
    public String delete(HttpSession session, @PathVariable String username) {
        User user = (User) session.getAttribute("user");
        User profile = userService.getUserByUsername(username);
        userService.delete(profile.getId(), user);
        return "redirect:/logout";
    }
}


