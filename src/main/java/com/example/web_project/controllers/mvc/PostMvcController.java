package com.example.web_project.controllers.mvc;

import com.example.web_project.models.*;
import com.example.web_project.models.dtos.*;
import com.example.web_project.services.CommentService;
import com.example.web_project.services.PostService;
import com.example.web_project.services.UserService;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.mappers.CommentMapper;
import com.example.web_project.utils.mappers.PostMapper;
import com.example.web_project.utils.mappers.TagMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/posts")
public class PostMvcController {

    public final UserService userService;
    private final PostService postService;
    private final CommentService commentService;
    private final PostMapper postMapper;
    private final CommentMapper commentMapper;

    private final TagMapper tagMapper;

    public PostMvcController(UserService userService, PostService postService, CommentService commentService, PostMapper postMapper, CommentMapper commentMapper, TagMapper tagMapper) {
        this.userService = userService;
        this.postService = postService;
        this.commentService = commentService;
        this.postMapper = postMapper;
        this.commentMapper = commentMapper;
        this.tagMapper = tagMapper;
    }

    @ModelAttribute("allTags")
    public List<Tag> populateTags() {
        return postService.getAllTags();
    }

    @ModelAttribute("allUsers")
    public List<User> populateUsers(UserFilterOptions userFilterOptions) {
        return userService.getAll(userFilterOptions);
    }

    @ModelAttribute("allPosts")
    public List<Post> populatePosts(PostFilterOptions postFilterOptions) {
        return postService.getAll(postFilterOptions);
    }

    @ModelAttribute("sortPost")
    public List<String> populateSortParams(){
        return new ArrayList<>(List.of("username_ASC", "username_DESC", "title_ASC", "title_DESC"));
    }

    @ModelAttribute("isAuth")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("user") != null;
    }

    @GetMapping("/{id}")
    public String getPost(@PathVariable int id, HttpSession session, Model model) {
        try {
            Post post = postService.getPostById(id);
            model.addAttribute("loggedUser",(User) session.getAttribute("user"));
            model.addAttribute("post", post);
            model.addAttribute("commentToCreate",new CommentDtoIn());
        }
        catch(EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        return "PostView";
    }

    @GetMapping
    public String getPosts(Model model, PostFilterOptions postFilterOptions) {
        List<Post> posts = postService.getAll(postFilterOptions);
        model.addAttribute("posts", posts);
        model.addAttribute("postFilterDto", new PostFilterDto());
        return "AllPosts";
    }

    @PostMapping("/{id}")
    public String createComment(HttpSession session, @PathVariable int id,
                                @Valid @ModelAttribute CommentDtoIn commentToCreate,
                                BindingResult bindingResult,
                                Model model) {

        if(bindingResult.hasErrors()) {
            return "PostView";
        }
        try {
            User user = (User) session.getAttribute("user");
            Comment comment = commentMapper.fromDtoCreate(id, commentToCreate, user);
            commentService.create(comment);
            return "redirect:/posts/" + id + "#comment" + comment.getId();
        } catch (Exception e) {
            return "redirect:/posts/" + id;
        }

    }

    @PostMapping("/{id}/like")
    public String likePost(HttpSession session, @PathVariable int id) {

        User user = (User)session.getAttribute("user");
        Post post = postService.getPostById(id);
        postService.addVote(user,post,1);

        return "redirect:/posts/"  + id;
    }

    @PostMapping("/{id}/dislike")
    public String dislikePost(HttpSession session, @PathVariable int id) {

        User user = (User)session.getAttribute("user");
        Post post = postService.getPostById(id);
        postService.addVote(user,post,2);

        return "redirect:/posts/"  + id;
    }

    @PostMapping("/{id}/vote/remove")
    public String removeVotePost(HttpSession session, @PathVariable int id) {
        User user = (User)session.getAttribute("user");
        Post post = postService.getPostById(id);
        postService.removeVote(user,post);

        return "redirect:/posts/"  + id;
    }

    @PostMapping("/{id}/{commentId}/like")
    public String likeComment(HttpSession session, @PathVariable int id, @PathVariable int commentId) {
        User user = (User)session.getAttribute("user");

        Comment comment = commentService.getCommentById(commentId);
        commentService.addVote(comment,user,1);

        return "redirect:/posts/"  + id;
    }

    @PostMapping("/{id}/{commentId}/dislike")
    public String dislikeComment(HttpSession session, @PathVariable int id, @PathVariable int commentId) {

        User user = (User)session.getAttribute("user");

        Comment comment = commentService.getCommentById(commentId);
        commentService.addVote(comment,user,2);

        return "redirect:/posts/"  + id;
    }

    @PostMapping("/{id}/{commentId}/vote/remove")
    public String removeVoteComment(HttpSession session, @PathVariable int id, @PathVariable int commentId) {
        User user = (User)session.getAttribute("user");
        Comment comment = commentService.getCommentById(commentId);
        commentService.removeVote(comment,user);

        return "redirect:/posts/"  + id;
    }

    @GetMapping("/{id}/edit")
    public String editPostView(HttpSession session, @PathVariable int id, Model model) {
        User user = (User)session.getAttribute("user");
        if(!populateIsAuthenticated(session) || user.isBlocked()) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You're not allowed to use this resource");
        }
        try {
            Post post = postService.getPostById(id);
            PostDtoEdit postDtoEdit = postMapper.editExisting(post);
            model.addAttribute("post", postDtoEdit);
            model.addAttribute("existingPost", post);
        }
        catch(EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "EditPost";
    }


    @PostMapping("/{id}/edit")
    public String editPost(HttpSession session, @PathVariable int id, @Valid @ModelAttribute("post") PostDtoEdit post, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "EditPost";
        }
        User user = (User) session.getAttribute("user");
        Post postToEdit = postService.getPostById(id);
        postToEdit = postMapper.fromDtoUpdate(id,post);
        postService.update(postToEdit,user);
        return "redirect:/posts/" + id;
    }

    //TODO make verification if there is loggedUser
    @GetMapping("/create")
    public String createPostView(HttpSession session, Model model) {
        User user = (User)session.getAttribute("user");
        if(!populateIsAuthenticated(session) || (user.isBlocked())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You're not allowed to use this resource");
        }
        model.addAttribute("post", new PostDtoEdit());

        return "CreatePost";
    }

    //TODO Implement Error Handling
    //TODO make verification if there is loggedUser
    @PostMapping("/create")
    public String createPost(HttpSession session, @Valid @ModelAttribute("post") PostDtoEdit post, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "CreatePost";
        }
        User user = (User) session.getAttribute("user");
        List<Tag> tags = post.getTags().stream()
                .map(tagMapper::fromTagName).collect(Collectors.toList());
        Post postToCreate = postMapper.fromDtoCreate(post, user);
        postService.create(postToCreate);
        postService.addTags(user,postToCreate,tags);

        return "redirect:/posts/"+postToCreate.getId();
    }

    @PostMapping("/{id}/delete")
    public String deletePost(HttpSession session, @PathVariable int id) {
        User user = (User) session.getAttribute("user");
        Post postToDelete = postService.getPostById(id);
        postService.delete(postToDelete.getId(),user);

        return "redirect:/";
    }


    @PostMapping("/{id}/{commentId}/delete")
    public String deleteComment(HttpSession session, @PathVariable int id, @PathVariable int commentId) {
        User user = (User) session.getAttribute("user");
        Comment comment = commentService.getCommentById(commentId); // to check if exists
        commentService.delete(commentId,user);

        return "redirect:/posts/" + id;
    }


    @GetMapping("/{id}/{commentId}/edit")
    public String editCommentView(@PathVariable int id,@PathVariable int commentId, Model model) {
        Comment comment = commentService.getCommentById(commentId);
        CommentDtoEdit commentDtoEdit = commentMapper.toEditDto(comment);
        model.addAttribute("comment", commentDtoEdit);
        model.addAttribute("post",postService.getPostById(id));

        return "EditComment";
    }

    @PostMapping("/{id}/{commentId}/edit")
    public String editComment(HttpSession session, @PathVariable int id,@PathVariable int commentId, @Valid @ModelAttribute("comment") CommentDtoIn comment) {
        Comment updatedComment = commentMapper.fromDtoUpdate(commentId, comment);
        User user = (User) session.getAttribute("user");
        commentService.update(updatedComment,user);

        return "redirect:/posts/" + id + "#comment" + commentId;
    }

    @GetMapping("/user/{username}")
    public String userPosts(Model model, @PathVariable String username) {
        User user = userService.getUserByUsername(username);
        List<Post> posts = new ArrayList<>(user.getPosts());
        model.addAttribute("posts", posts);
        model.addAttribute("postFilterDto", new PostFilterDto());
        return "AllPosts";
    }

    @PostMapping()
    public String filter(HttpSession session, @ModelAttribute PostFilterDto postFilterDto, Model model) {
        User user = (User) session.getAttribute("user");
        model.addAttribute("posts", postService.filter(postFilterDto));
        model.addAttribute("currentUser", user);
        model.addAttribute("postFilterDto", new PostFilterDto());
        return "AllPosts";
    }
}

