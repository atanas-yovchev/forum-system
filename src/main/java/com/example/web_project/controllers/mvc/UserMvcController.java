package com.example.web_project.controllers.mvc;

import com.example.web_project.models.User;
import com.example.web_project.models.UserFilterOptions;
import com.example.web_project.models.dtos.UserFilterDto;
import com.example.web_project.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final UserService userService;

    public UserMvcController(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("isAuth")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("user") != null;
    }

    @ModelAttribute("sortUsers")
    public List<String> populateSortParams(){
        return new ArrayList<>(List.of("username_ASC", "username_DESC", "firstName_ASC", "firstName_DESC",
                "lastName_ASC", "lastName_DESC", "email_ASC", "email_DESC"));
    }

    @ModelAttribute("allUsers")
    public List<User> populateUsers(UserFilterOptions userFilterOptions) {
        return userService.getAll(userFilterOptions);
    }

    @GetMapping
    public String showNumberOfUsers(Model model,
                                    UserFilterOptions userFilterOptions){
        List<User> users = userService.getAll(userFilterOptions);
        model.addAttribute("userFilterDto", new UserFilterDto());
        model.addAttribute("users", users);
        return "UsersView";
    }

    @PostMapping()
    public String filter(HttpSession session, @ModelAttribute UserFilterDto userFilterDto, Model model) {
        User user = (User) session.getAttribute("user");
        model.addAttribute("users", userService.filter(userFilterDto));
        model.addAttribute("currentUser", user);
        model.addAttribute("userFilterDto", new UserFilterDto());
        return "UsersView";
    }
}
