package com.example.web_project.controllers.mvc;

import com.example.web_project.models.Post;
import com.example.web_project.models.PostFilterOptions;
import com.example.web_project.models.User;
import com.example.web_project.models.UserFilterOptions;
import com.example.web_project.services.PostService;
import com.example.web_project.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping
public class HomeMvcController {

    private final PostService postService;
    private final UserService userService;

    public HomeMvcController(PostService postService, UserService userService) {
        this.postService = postService;
        this.userService = userService;
    }
    @ModelAttribute("isAuth")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("user") != null;
    }


    @GetMapping()
    public String showHomePage(HttpSession session,
                               Model model,
                               UserFilterOptions userFilterOptions,
                               PostFilterOptions postFilterOptions){
        List<Post> posts = postService.getTenMostCommentedPosts();
        List<Post> tenLastCreatedPosts = postService.getTenLastCreatedPosts();
        List<Post> allPosts = postService.getAll(postFilterOptions);
        List<User> users = userService.getAll(userFilterOptions);
        model.addAttribute("usersCount", String.valueOf(users.size()));
        model.addAttribute("postsCount", String.valueOf(allPosts.size()));
        model.addAttribute("mostCommentedPosts", posts);
        model.addAttribute("lastCreatedPosts", tenLastCreatedPosts);
        model.addAttribute("loggedUser",(User) session.getAttribute("user"));
        return "Home";
    }
}
