package com.example.web_project.controllers.rest;

import com.example.web_project.models.Comment;
import com.example.web_project.models.User;
import com.example.web_project.models.dtos.CommentDtoIn;
import com.example.web_project.models.dtos.CommentDtoOut;
import com.example.web_project.services.CommentService;
import com.example.web_project.utils.exceptions.BlockedUserException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import com.example.web_project.utils.helpers.AuthenticationHelper;
import com.example.web_project.utils.mappers.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/{postId}/comments")
public class CommentController {

    private final CommentService service;
    private final CommentMapper mapper;
    private final AuthenticationHelper authHelper;

    @Autowired
    public CommentController(CommentService service, CommentMapper mapper, AuthenticationHelper authHelper) {
        this.service = service;
        this.mapper = mapper;
        this.authHelper = authHelper;
    }

    @GetMapping
    public List<CommentDtoOut> get(@PathVariable Optional<Integer> postId,
                                   @RequestParam(required = false) Optional<String> username,
                                   @RequestParam(required = false) Optional<String> content,
                                   @RequestParam(required = false) Optional<String> sort) {
        try {
            return service.getAll(username, content, postId, sort).stream()
                    .map(mapper::toDto)
                    .collect(Collectors.toList());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{commentId}")
    public CommentDtoOut get(@PathVariable int commentId) {
        try {
            Comment comment = service.getCommentById(commentId);
            return mapper.toDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public CommentDtoOut create(@RequestHeader HttpHeaders headers,
                                @PathVariable int postId,
                                @Valid @RequestBody CommentDtoIn commentDtoIn) {
        try {
            User user = authHelper.tryGetUser(headers);
            Comment comment = mapper.fromDtoCreate(postId, commentDtoIn, user);
            service.create(comment);
            return mapper.toDto(comment);
        } catch (BlockedUserException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{commentId}")
    public CommentDtoOut update(@RequestHeader HttpHeaders headers,
                                @PathVariable int commentId,
                                @Valid @RequestBody CommentDtoIn commentDtoIn) {
        try {
            User user = authHelper.tryGetUser(headers);
            Comment commentToUpdate = mapper.fromDtoUpdate(commentId, commentDtoIn);
            service.update(commentToUpdate, user);
            return mapper.toDto(commentToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException | BlockedUserException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{commentId}")
    public CommentDtoOut delete(@RequestHeader HttpHeaders headers,
                                @PathVariable int commentId) {
        try {
            User user = authHelper.tryGetUser(headers);
            Comment commentToDelete = service.delete(commentId, user);
            return mapper.toDto(commentToDelete);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{commentId}/vote")
    public CommentDtoOut addVote(@RequestHeader HttpHeaders headers,
                                 @PathVariable int commentId,
                                 @RequestParam int vote) {
        try {
            User user = authHelper.tryGetUser(headers);
            Comment comment = service.getCommentById(commentId);
            service.addVote(comment, user, vote);
            return mapper.toDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{commentId}/vote")
    public CommentDtoOut removeVote(@RequestHeader HttpHeaders headers,
                                    @PathVariable int commentId) {
        try {
            User user = authHelper.tryGetUser(headers);
            Comment comment = service.getCommentById(commentId);
            service.removeVote(comment, user);
            return mapper.toDto(comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}