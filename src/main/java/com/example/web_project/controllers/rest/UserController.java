package com.example.web_project.controllers.rest;

import com.example.web_project.models.User;
import com.example.web_project.models.UserFilterOptions;
import com.example.web_project.models.dtos.RegisterDto;
import com.example.web_project.models.dtos.UpdateUserDto;
import com.example.web_project.models.dtos.UserDtoOut;
import com.example.web_project.services.UserService;
import com.example.web_project.utils.exceptions.DuplicateEntityException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.SoftDeletedEntityException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import com.example.web_project.utils.helpers.AuthenticationHelper;
import com.example.web_project.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService service;
    private final UserMapper mapper;
    private final AuthenticationHelper authHelper;

    @Autowired
    public UserController(UserService service,
                          UserMapper mapper,
                          AuthenticationHelper authHelper) {
        this.service = service;
        this.mapper = mapper;
        this.authHelper = authHelper;
    }

    @GetMapping()
    public List<UserDtoOut> get(UserFilterOptions userFilterOptions) {
        try {
            return service.getAll(userFilterOptions).stream()
                    .map(mapper::toDto)
                    .collect(Collectors.toList());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public UserDtoOut get(@PathVariable int id) {
        try {
            return mapper.toDto(service.getUserById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public UserDtoOut create(@Valid @RequestBody RegisterDto registerDto) {
        try {
            return mapper.toDto(service.create(mapper.fromDtoCreate(registerDto)));
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (SoftDeletedEntityException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public UserDtoOut update(@RequestHeader HttpHeaders headers,
                             @PathVariable int id,
                             @Valid @RequestBody UpdateUserDto userDtoInUpdate) {
        try {
            User authenticatedUser = authHelper.tryGetUser(headers);
            User updatedUser = mapper.fromDtoUpdate(authenticatedUser, userDtoInUpdate);
            service.update(updatedUser, authenticatedUser);
            return mapper.toDto(updatedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public UserDtoOut delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authHelper.tryGetUser(headers);
            User deletedUser = service.delete(id, authenticatedUser);
            return mapper.toDto(deletedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/admin/promote")
    public UserDtoOut addAdminRole(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authHelper.tryGetUser(headers);
            User promotedUser = service.addAdminRole(authenticatedUser, id);
            return mapper.toDto(promotedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/admin/demote")
    public UserDtoOut removeAdminRole(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authHelper.tryGetUser(headers);
            User demotedUser = service.removeAdminRole(authenticatedUser, id);
            return mapper.toDto(demotedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/blocked")
    public UserDtoOut blockOrUnblock(@RequestHeader HttpHeaders headers,
                                     @RequestParam String status,
                                     @PathVariable int id) {
        try {
            User authenticatedUser = authHelper.tryGetUser(headers);
            User user = service.changeBlockedStatus(authenticatedUser, id, status);
            return mapper.toDto(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
