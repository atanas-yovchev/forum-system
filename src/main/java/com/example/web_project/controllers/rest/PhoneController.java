package com.example.web_project.controllers.rest;

import com.example.web_project.models.User;
import com.example.web_project.models.Phone;
import com.example.web_project.services.PhoneService;
import com.example.web_project.utils.exceptions.DuplicateEntityException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import com.example.web_project.utils.helpers.AuthenticationHelper;
import com.example.web_project.utils.mappers.PhoneMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/users/{userId}/phone")
public class PhoneController {
    private final PhoneService phoneService;
    private final PhoneMapper phoneMapper;

    private final AuthenticationHelper authenticationHelper;

    public PhoneController(PhoneService phoneService, PhoneMapper phoneMapper, AuthenticationHelper authenticationHelper) {
        this.phoneService = phoneService;
        this.phoneMapper = phoneMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public Phone get(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            return phoneMapper.getExistingFromId(userId, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
        catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PostMapping
    public Phone create(@RequestHeader HttpHeaders headers, @PathVariable int userId, @RequestParam String phoneNumber) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Phone phone = phoneMapper.fromStringToCreate(userId,phoneNumber);
            return phoneService.create(phone, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
        catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
        }
        catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @PutMapping
    public Phone update(@RequestHeader HttpHeaders headers, @PathVariable int userId, @RequestParam String phoneNumber) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Phone phone = phoneMapper.fromStringToUpdate(userId,loggedUser,phoneNumber);
            return phoneService.update(phone, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
        catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
        }
        catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @DeleteMapping
    public Phone delete(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User loggedUser = authenticationHelper.tryGetUser(headers);
            Phone phone = phoneMapper.getExistingFromId(userId, loggedUser);
            return phoneService.delete(phone, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
        catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }


}
