package com.example.web_project.controllers.rest;

import com.example.web_project.models.Post;
import com.example.web_project.models.PostFilterOptions;
import com.example.web_project.models.Tag;
import com.example.web_project.models.User;
import com.example.web_project.models.dtos.PostDtoIn;
import com.example.web_project.models.dtos.PostDtoOut;
import com.example.web_project.models.dtos.TagDto;
import com.example.web_project.services.PostService;
import com.example.web_project.utils.exceptions.BlockedUserException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import com.example.web_project.utils.helpers.AuthenticationHelper;
import com.example.web_project.utils.mappers.PostMapper;
import com.example.web_project.utils.mappers.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/posts")
public class PostController {
    private final PostService service;
    private final AuthenticationHelper authHelper;
    private final PostMapper postMapper;

    private final TagMapper tagMapper;

    @Autowired
    public PostController(PostService service, PostMapper postMapper, AuthenticationHelper authHelper, TagMapper tagMapper) {
        this.service = service;
        this.postMapper = postMapper;
        this.authHelper = authHelper;
        this.tagMapper = tagMapper;
    }

    @GetMapping
    public List<PostDtoOut> get(@RequestParam(required = false) PostFilterOptions postFilterOptions) {
        try {
            return service.getAll(postFilterOptions).stream()
                    .map(postMapper::toDto)
                    .collect(Collectors.toList());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @GetMapping("/{id}")
    public PostDtoOut get(@PathVariable int id) {
        try {
            return postMapper.toDto(service.getPostById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public PostDtoOut create(@RequestHeader HttpHeaders headers,
                             @Valid @RequestBody PostDtoIn postDtoIn) {
        try {
            User user = authHelper.tryGetUser(headers);
            Post post = postMapper.fromDtoCreate(postDtoIn, user);
            service.create(post);
            return postMapper.toDto(post);
        } catch (BlockedUserException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public PostDtoOut update(@RequestHeader HttpHeaders headers,
                             @PathVariable int id,
                             @Valid @RequestBody PostDtoIn postDtoIn) {
        try {
            User loggedUser = authHelper.tryGetUser(headers);
            Post post = postMapper.fromDtoUpdate(id,postDtoIn);
            service.update(post, loggedUser);
            return postMapper.toDto(service.getPostById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException | BlockedUserException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public PostDtoOut delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authHelper.tryGetUser(headers);
            Post deletedPost = service.delete(id, user);
            return postMapper.toDto(deletedPost);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{postId}/vote")
    public PostDtoOut addVote(@RequestHeader HttpHeaders headers,
                              @RequestParam int vote,
                              @PathVariable int postId) {
        try {
            User user = authHelper.tryGetUser(headers);
            Post post = service.getPostById(postId);
            service.addVote(user, post, vote);
            return postMapper.toDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{postId}/vote")
    public PostDtoOut removeVote(@RequestHeader HttpHeaders headers,
                                 @PathVariable int postId) {
        try {
            User user = authHelper.tryGetUser(headers);
            Post post = service.getPostById(postId);
            service.removeVote(user, post);
            return postMapper.toDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{postId}/tags")
    public PostDtoOut addTags(@RequestHeader HttpHeaders headers,
                              @PathVariable int postId,
                              @Valid @RequestBody TagDto tagDto) {
        try {
            User loggedUser = authHelper.tryGetUser(headers);
            Post post = service.getPostById(postId);
            List<Tag> tags = tagDto.getTagNames().stream()
                    .map(tagMapper::fromTagName).collect(Collectors.toList());
            if(!tags.isEmpty()) {
                service.addTags(loggedUser, post, tags);
            }
            return postMapper.toDto(post);
        }
        catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/{postId}/tags/{tagId}")
    public PostDtoOut removeTag(@RequestHeader HttpHeaders headers,
                                @PathVariable int postId,
                                @PathVariable int tagId) {
        try {
            User loggedUser = authHelper.tryGetUser(headers);
            Post post = service.getPostById(postId);
            service.removeTag(loggedUser, post, tagId);
            return postMapper.toDto(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/admin/tags/{tagId}")
    public String deleteTag(@RequestHeader HttpHeaders headers,
                            @PathVariable int tagId) {
        try {
            User loggedUser = authHelper.tryGetUser(headers);
            return service.deleteTag(loggedUser, tagId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}