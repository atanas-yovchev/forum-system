package com.example.web_project.repositories;

import com.example.web_project.models.User;
import com.example.web_project.models.UserFilterOptions;
import com.example.web_project.models.dtos.UserFilterDto;

import java.util.List;

public interface UserRepository {

    List<User> getAll(UserFilterOptions userFilterOptions);

    User getUserById(int id);

    User getUserByUsername(String username);

    User getUserByEmail(String email);

    User create(User user);

    User update(User user);

    User delete(User user);
    boolean isUserSoftDeleted(String email, String username);

    List<User> filter(UserFilterDto userFilterDto);
}
