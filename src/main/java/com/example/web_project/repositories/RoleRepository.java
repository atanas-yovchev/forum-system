package com.example.web_project.repositories;

import com.example.web_project.models.Role;

public interface RoleRepository {

    Role getRoleType(String roleName);
}
