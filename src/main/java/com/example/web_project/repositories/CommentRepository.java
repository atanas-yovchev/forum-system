package com.example.web_project.repositories;

import com.example.web_project.models.Comment;

import java.util.List;
import java.util.Optional;

public interface CommentRepository {

    List<Comment> getAll(Optional<String> username,
                         Optional<String> content,
                         Optional<Integer> postId,
                         Optional<String> sort);

    Comment getCommentById(int commentId);

    Comment create(Comment comment);

    Comment update(Comment comment);

    Comment delete(int id);
}
