package com.example.web_project.repositories;

import com.example.web_project.models.Post;
import com.example.web_project.models.PostFilterOptions;
import com.example.web_project.models.Tag;
import com.example.web_project.models.dtos.PostFilterDto;

import java.util.List;

public interface PostRepository {
    List<Post> getAll(PostFilterOptions postFilterOptions);
    Post getPostById(int id);
    Post create(Post post);
    Post update(Post post);
    Post delete(Post post);

    List<Post> getAllByTagName(String tagName);
    List<Post> getTenMostCommentedPosts();
    List<Post> getTenLastCreatedPosts();

    List<Tag> getAllTags();
    List<Post> filter(PostFilterDto postFilterDto);
}
