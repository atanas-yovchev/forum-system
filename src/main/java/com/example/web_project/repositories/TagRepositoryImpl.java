package com.example.web_project.repositories;

import com.example.web_project.models.Tag;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private final SessionFactory sessionFactory;

    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public Tag create(Tag tag) {
        try(Session session = sessionFactory.openSession()) {
            session.save(tag);
            return tag;
        }
    }

    @Override
    public Tag update(Tag tag) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
            return tag;
        }
    }

    @Override
    public Tag delete(Tag tag) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(tag);
            session.getTransaction().commit();
            return tag;
        }
    }

    @Override
    public Tag getByName(String tagName) {
        try(Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name = :tagName",
                    Tag.class);
            query.setParameter("tagName", tagName);

            List<Tag> result = query.list();
            if(result.isEmpty()) {
                throw new EntityNotFoundException("Tag", "name", tagName);
            }

            return result.get(0);
        }
    }

    @Override
    public Tag getById(int id) {
        try(Session session = sessionFactory.openSession()) {
            Optional<Tag> tag = Optional.ofNullable(session.get(Tag.class, id));

            if(tag.isEmpty()) {
                throw new EntityNotFoundException("Tag", id);
            }

            return tag.get();
        }
        }
}
