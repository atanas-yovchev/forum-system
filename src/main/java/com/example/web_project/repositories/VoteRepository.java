package com.example.web_project.repositories;

import com.example.web_project.models.Vote;

public interface VoteRepository {
    Vote getById(int id);
}
