package com.example.web_project.repositories;

import com.example.web_project.models.User;
import com.example.web_project.models.Phone;

public interface PhoneRepository {

    Phone create(Phone phone);

    Phone update(Phone phone);

    Phone delete(Phone phone);

    Phone getPhoneByNumber(String phoneNumber);

    Phone getPhoneByUser(User user);
}
