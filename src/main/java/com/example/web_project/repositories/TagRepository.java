package com.example.web_project.repositories;

import com.example.web_project.models.Tag;

public interface TagRepository {

    Tag create(Tag tag);

    Tag update(Tag tag);

    Tag delete(Tag tag);

    Tag getByName(String tagName);

    Tag getById(int id);


}
