package com.example.web_project.repositories;

import com.example.web_project.models.Comment;
import com.example.web_project.models.Post;
import com.example.web_project.models.PostFilterOptions;
import com.example.web_project.models.Tag;
import com.example.web_project.models.dtos.PostFilterDto;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PostRepositoryImpl implements PostRepository {

    private final SessionFactory sessionFactory;

    public PostRepositoryImpl(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Post> getAll(PostFilterOptions postFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" select p from Post p ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            postFilterOptions.getSearch().ifPresent(value -> {
                filters.add(" p.title like :search or p.creator.username like :search ");
                params.put("search", "%" + value + "%");
            });

            postFilterOptions.getTag().ifPresent(value -> {
                queryString.append(" inner join p.tags t ");
                filters.add(" t.name like :tagName ");
                params.put("tagName", "%" + value + "%");
            });

            postFilterOptions.getUsername().ifPresent(value -> {
                filters.add(" p.creator.username like :username ");
                params.put("username", "%" + value + "%");
            });

            postFilterOptions.getTitle().ifPresent(value -> {
                filters.add(" p.title like :title ");
                params.put("title", "%" + value + "%");
            });

            if (!filters.isEmpty()) {
                queryString.append(" where ")
                        .append(String.join(" and ", filters))
                        .append(" and p.creator.isDeleted = false ");
            } else {
                queryString.append(" where p.creator.isDeleted = false ");
            }
            postFilterOptions.getSort().ifPresent(value -> queryString.append(generateStringFromSort(value)));

            Query<Post> query = session.createQuery(queryString.toString(), Post.class);
            query.setProperties(params);

            List<Post> posts = query.list();
            for (Post post : posts) {
                int postId = post.getId();
                Query<Comment> commentQuery = session.createQuery("select c from Comment c where " +
                        "c.post.id = :postId and c.creator.isDeleted = false ", Comment.class);
                commentQuery.setParameter("postId", postId);
                List<Comment> comments = commentQuery.list();
                post.setComments(new HashSet<>(comments));
            }

            return posts;
        }
    }

    private String generateStringFromSort(String value) {
        value = value.trim();
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty() || params.length != 2 || params[0].isEmpty() || params[1].isEmpty()) {
            throw new UnsupportedOperationException("Sort should have exactly two params divided by _ symbol " +
                    "(Ex: username_asc)");
        }

        switch (params[0]) {
            case "username":
                queryString.append(" creator.username ");
                break;
            case "title":
                queryString.append(" title ");
                break;
            default:
                throw new UnsupportedOperationException("Invalid Sort type parameter");
        }

        if (params[1].equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }

        return queryString.toString();
    }

    @Override
    public Post getPostById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> postQuery = session.createQuery("select p from Post p where p.id = :id and p.creator.isDeleted = false ", Post.class);
            postQuery.setParameter("id", id);
            List<Post> posts = postQuery.list();

            if (posts.isEmpty()) {
                throw new EntityNotFoundException("Post", id);
            }
            Query<Comment> commentQuery = session.createQuery(
                    "select c from Comment c where c.post.id = :id and c.creator.isDeleted = false ",
                    Comment.class);

            commentQuery.setParameter("id", id);
            List<Comment> comments = commentQuery.list();

            Post post = posts.get(0);
            post.setComments(new HashSet<>(comments));

            return post;
        }
    }

    @Override
    public List<Post> getAllByTagName(String tagName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery(
                    "select p as tags from Post p inner join p.tags t where t.name like :tagName " +
                            "and p.creator.isDeleted = false ", Post.class);
            query.setParameter("tagName", tagName);

            return query.list();
        }
    }

    @Override
    public List<Post> getTenMostCommentedPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("select p from Post p order by p.comments.size desc",
                    Post.class);
            query.setMaxResults(10);

            return query.list();
        }
    }

    @Override
    public List<Post> getTenLastCreatedPosts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("select p from Post p order by p.creationDate desc",
                    Post.class);
            query.setMaxResults(10);

            return query.list();
        }
    }

    @Override
    public List<Tag> getAllTags() {
        try(Session session =  sessionFactory.openSession()) {
           Query<Tag> query = session.createQuery("from Tag", Tag.class);
            return query.list();
        }
    }

    @Override
    public Post create(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.save(post);
            return post;
        }
    }

    @Override
    public Post update(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
            return post;
        }
    }

    @Override
    public Post delete(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(post);
            session.getTransaction().commit();
            return post;
        }
    }

    @Override
    public List<Post> filter(PostFilterDto postFilterDto) {
        PostFilterOptions postFilterOptions = new PostFilterOptions();

        postFilterOptions.setSearch(Optional.of(postFilterDto.getSearch().get()));
        postFilterOptions.setSort(Optional.of(postFilterDto.getSortPost().get()));

        return getAll(postFilterOptions);
    }
}
