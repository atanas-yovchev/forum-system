package com.example.web_project.repositories;

import com.example.web_project.models.Role;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

    private final SessionFactory sessionFactory;

    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Role getRoleType(String roleName) {
        roleName = roleName.toLowerCase();
        try(Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role where name like :rolename", Role.class);
            query.setParameter("rolename", roleName);
            List<Role> roles = query.list();
            if(roles.isEmpty()) {
               throw new EntityNotFoundException("Role", "name", roleName);
            }
            return roles.get(0);
        }
    }
}
