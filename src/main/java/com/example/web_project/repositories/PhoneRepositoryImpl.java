package com.example.web_project.repositories;

import com.example.web_project.models.User;
import com.example.web_project.models.Phone;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhoneRepositoryImpl implements PhoneRepository {

    private final SessionFactory sessionFactory;

    public PhoneRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Phone create(Phone phone) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(phone);
            session.getTransaction().commit();
            return phone;
        }
    }

    @Override
    public Phone update(Phone phone) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(phone);
            session.getTransaction().commit();
            return phone;
        }
    }



    @Override
    public Phone delete(Phone phone) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(phone);
            session.getTransaction().commit();
            return phone;
        }
    }

    @Override
    public Phone getPhoneByNumber(String phoneNumber) {
        try(Session session = sessionFactory.openSession()) {
           Query<Phone> query =  session.createQuery("from Phone where phoneNumber = :phoneNumber", Phone.class);
            query.setParameter("phoneNumber",phoneNumber);
            List<Phone> result = query.list();

            if(result.isEmpty()) {
                throw new EntityNotFoundException("Phone", "number", phoneNumber);
            }

            return result.get(0);
        }
    }

    @Override
    public Phone getPhoneByUser(User user) {
        try(Session session = sessionFactory.openSession()) {
            Query<Phone> query =  session.createQuery("from Phone where user = :user", Phone.class);
            query.setParameter("user", user);
            List<Phone> result = query.list();

            if(result.isEmpty()) {
                throw new EntityNotFoundException(String.format("No phones for user with id %d were found.",user.getId()));
            }

            return result.get(0);
        }
    }
}
