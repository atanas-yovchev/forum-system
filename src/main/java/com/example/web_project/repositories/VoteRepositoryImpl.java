package com.example.web_project.repositories;

import com.example.web_project.models.Vote;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VoteRepositoryImpl implements VoteRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public VoteRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Vote getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Vote vote = session.get(Vote.class, id);

            if (vote == null) {
                throw new EntityNotFoundException("Vote", id);
            }
            
            return vote;
        }
    }
}
