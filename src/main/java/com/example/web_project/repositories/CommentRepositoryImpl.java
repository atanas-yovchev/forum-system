package com.example.web_project.repositories;

import com.example.web_project.models.Comment;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class CommentRepositoryImpl implements CommentRepository {

    private final SessionFactory sessionFactory;

    public CommentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    // TODO optimization for hiding deleted users
    @Override
    public List<Comment> getAll(Optional<String> username,
                                Optional<String> content,
                                Optional<Integer> postId,
                                Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" select c from Comment c ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            username.ifPresent(value -> {
                filters.add(" c.creator.username like :username ");
                params.put("username", "%" + value + "%");
            });

            content.ifPresent(value -> {
                filters.add(" c.content like :content ");
                params.put("content", "%" + value + "%");
            });

            postId.ifPresent(value -> {
                filters.add(" c.post.id = :postId ");
                params.put("postId", value);
            });

            if (!filters.isEmpty()) {
                queryString.append(" where ")
                        .append(String.join(" and ", filters))
                        .append(" and c.creator.isDeleted = false ");
            } else {
                queryString.append(" where c.creator.isDeleted = false ");
            }
            sort.ifPresent(value -> queryString.append(generateStringFromSort(value)));

            Query<Comment> query = session.createQuery(queryString.toString(), Comment.class);
            query.setProperties(params);

            List<Comment> comments = query.list();
            comments.removeIf(comment -> comment.getPost().getCreator().isDeleted());
            return comments;
        }
    }

    private String generateStringFromSort(String value) {
        value = value.trim();
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty() || params.length != 2 || params[0].isEmpty() || params[1].isEmpty()) {
            throw new UnsupportedOperationException("Sort should have exactly two params divided by _ symbol " +
                    "(Ex: username_asc)");
        }

        switch (params[0]) {
            case "username":
                queryString.append(" creator.username ");
                break;
            case "content":
                queryString.append(" content ");
                break;
            default:
                throw new UnsupportedOperationException("Invalid Sort type parameter");
        }

        if (params[1].equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }

        return queryString.toString();
    }

    @Override
    public Comment getCommentById(int commentId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Comment> query = session.createQuery("select c from Comment c where c.id = :commentId " +
                    "and c.creator.isDeleted = false ", Comment.class);
            query.setParameter("commentId", commentId);
            List<Comment> comments = query.list();

            if (comments.isEmpty() || comments.get(0).getPost().getCreator().isDeleted()) {
                throw new EntityNotFoundException("Comment", commentId);
            }

            return comments.get(0);
        }
    }

    @Override
    public Comment create(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.save(comment);
            return comment;
        }
    }

    @Override
    public Comment update(Comment comment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(comment);
            session.getTransaction().commit();
            return comment;
        }
    }

    @Override
    public Comment delete(int id) {
        Comment comment = getCommentById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(comment);
            session.getTransaction().commit();
            return comment;
        }
    }
}
