package com.example.web_project.repositories;

import com.example.web_project.models.User;
import com.example.web_project.models.UserFilterOptions;
import com.example.web_project.models.dtos.UserFilterDto;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll(UserFilterOptions userFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from User ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            userFilterOptions.getSearch().ifPresent(value ->  {
                filters.add(" username like :search or firstName like :search or lastName like :search or email like :search ");
                params.put("search", "%" + value + "%");
            });
            userFilterOptions.getUsername().ifPresent(value -> {
                filters.add(" username like :username ");
                params.put("username", "%" + value + "%");
            });

            userFilterOptions.getFirstName().ifPresent(value -> {
                filters.add(" firstName like :firstName ");
                params.put("firstName", "%" + value + "%");
            });

            userFilterOptions.getLastName().ifPresent(value -> {
                filters.add(" lastName like :lastName ");
                params.put("lastName", "%" + value + "%");
            });

            userFilterOptions.getEmail().ifPresent(value -> {
                filters.add(" email like :email ");
                params.put("email", "%" + value + "%");
            });

            if (!filters.isEmpty()) {
                queryString.append(" where ")
                        .append(String.join(" and ", filters))
                        .append(" and isDeleted = false ");
            } else {
                queryString.append(" where isDeleted = false ");
            }
            userFilterOptions.getSortUsers().ifPresent(value -> queryString.append(generateStringFromSort(value)));

            Query<User> query = session.createQuery(queryString.toString(), User.class);
            query.setProperties(params);

            return query.list();
        }
    }

    private String generateStringFromSort(String value) {
        value = value.trim();
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty() || params.length != 2 || params[0].isEmpty() || params[1].isEmpty()) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }
        switch (params[0]) {
            case "username":
                queryString.append(" username ");
                break;
            case "firstName":
                queryString.append(" firstName ");
                break;
            case "lastName":
                queryString.append(" lastName ");
                break;
            case "email":
                queryString.append(" email ");
                break;
            default:
                throw new UnsupportedOperationException("Invalid Sort type parameter");
        }

        if (params[1].equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }

        return queryString.toString();
    }

    @Override
    public User getUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where id = :id and isDeleted = false",
                    User.class);
            query.setParameter("id", id);

            List<User> user = query.list();

            if (user.isEmpty()) {
                throw new EntityNotFoundException("User", id);
            }
            return user.get(0);
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username and isDeleted = false"
                    , User.class);
            query.setParameter("username", username);

            List<User> user = query.list();

            if (user.isEmpty()) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return user.get(0);
        }
    }

    @Override
    public User getUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email and isDeleted = false",
                    User.class);
            query.setParameter("email", email);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return result.get(0);
        }
    }

    @Override
    public boolean isUserSoftDeleted(String email, String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where (email = :email or username = :username) " +
                    "and isDeleted = true", User.class);
            query.setParameter("email", email);
            query.setParameter("username", username);

            List<User> result = query.list();
            return result.size() != 0;
        }
    }

    @Override
    public User create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
            return user;
        }
    }

    @Override
    public User update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
            return user;
        }
    }


    @Override
    public User delete(User user) {
        user.setDeleted(true);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
            return user;
        }
    }

    @Override
    public List<User> filter(UserFilterDto userFilterDto) {
        UserFilterOptions userFilterOptions = new UserFilterOptions();

        userFilterOptions.setSearch(Optional.of(userFilterDto.getSearch().get()));
        userFilterOptions.setSortUsers(Optional.of(userFilterDto.getSortUser().get()));

        return getAll(userFilterOptions);
    }
}
