package com.example.web_project.services;

import com.example.web_project.models.User;
import com.example.web_project.repositories.PhoneRepository;
import com.example.web_project.repositories.RoleRepository;
import com.example.web_project.repositories.UserRepository;
import com.example.web_project.utils.exceptions.DuplicateEntityException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.web_project.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    private UserRepository mockUserRepository;
    @Mock
    private PhoneRepository mockPhoneRepository;
    @Mock
    private RoleRepository mockRoleRepository;
    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void getAll_Should_CallRepository() {
        // Arrange
        when(mockUserRepository.getAll(any()))
                .thenReturn(null);

        // Act
        userService.getAll(any());

        // Assert
        verify(mockUserRepository, times(1)).getAll(any());
    }

    @Test
    public void getUserById_Should_ReturnUser_When_UserWithUsernameExist() {
        // Arrange
        User mockUser = createMockUser();

        when(mockUserRepository.getUserById(anyInt()))
                .thenReturn(mockUser);

        // Act
        User mockResult = userService.getUserById(mockUser.getId());

        // Assert
        assertEquals(mockUser, mockResult);
    }

    @Test
    public void getUserByUsername_Should_ReturnUser_When_UserWithUsernameExist() {
        // Arrange
        User mockUser = createMockUser();

        when(mockUserRepository.getUserByUsername(anyString()))
                .thenReturn(mockUser);

        // Act
        User mockResult = userService.getUserByUsername(mockUser.getUsername());

        // Assert
        assertEquals(mockUser, mockResult);
    }

    @Test
    public void create_Should_ThrowException_When_UsernameNotUnique() {
        // Arrange
        User mockUser = createMockUser();

        User mockExistingUser = createMockUser();
        mockExistingUser.setId(2);
        mockExistingUser.setUsername(mockUser.getUsername());
        when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenReturn(mockExistingUser);

        // Act, Assert
        assertThrows(
                DuplicateEntityException.class,
                () -> userService.create(mockUser));
    }

    @Test
    public void create_Should_ThrowException_When_EmailNotUnique() {
        // Arrange
        User mockUser = createMockUser();

        when(mockUserRepository.getUserByUsername(anyString()))
                .thenThrow(DuplicateEntityException.class);

        User mockExistingUser = createMockUser();
        mockExistingUser.setUsername("mockUsername2");
        mockExistingUser.setEmail(mockUser.getEmail());

        // Act, Assert
        assertThrows(
                DuplicateEntityException.class,
                () -> userService.create(mockUser));
    }

    @Test
    public void create_Should_CallRepository_When_UsernameAndEmailAreUnique() {
        // Arrange
        User mockUser = createMockUser();

        when(mockUserRepository.getUserByUsername(anyString()))
                .thenThrow(EntityNotFoundException.class);

        when(mockUserRepository.getUserByEmail(anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        userService.create(mockUser);

        // Assert
        verify(mockUserRepository, times(1)).create(mockUser);
    }

    @Test
    public void removeAdminRole_Should_ThrowException_When_UserIsNotAuthenticatedAsAdmin() {
        // Arrange
        User mockUserToDemote = createMockUser();

        // Act, Assert
        assertThrows(
                UnauthorizedOperationException.class,
                () -> userService.removeAdminRole(mock(User.class), mockUserToDemote.getId()));
    }

    @Test
    public void removeAdminRole_Should_ThrowException_When_UserToDemoteIsNotAdmin() {
        // Arrange
        User mockUserToDemote = createMockUser();
        User mockUserAdmin = createMockAdmin();
        mockUserAdmin.setId(2);

        when(mockUserRepository.getUserById(mockUserToDemote.getId()))
                .thenReturn(mockUserToDemote);

        // Act, Assert
        assertThrows(
                DuplicateEntityException.class,
                () -> userService.removeAdminRole(mockUserAdmin, mockUserToDemote.getId()));
    }

    @Test
    public void removeAdminRole_Should_Delete_PhoneNumber_When_UserToDemoteIsAdmin() {
        // Arrange
        User mockUserToDemote = createMockAdmin();

        // Act
        mockPhoneRepository.delete(mockUserToDemote.getPhone());

        // Assert
        assertNull(mockUserToDemote.getPhone());
        verify(mockPhoneRepository, times(1))
                .delete(mockUserToDemote.getPhone());
    }

    @Test
    public void removeAdminRole_Should_CallRepository_When_UserToDemoteIsAdmin() {
        // Arrange
        User mockUserToDemote = createMockAdmin();
        User mockUserAdmin = createMockAdmin();
        mockUserAdmin.setId(2);

        when(mockUserRepository.getUserById(mockUserToDemote.getId()))
                .thenReturn(mockUserToDemote);

        // Act
        userService.removeAdminRole(mockUserAdmin, mockUserToDemote.getId());

        // Assert
        assertFalse(mockUserToDemote.getRoles().stream().noneMatch(role -> role.getName().equalsIgnoreCase("admin")));
        verify(mockUserRepository, times(1))
                .update(mockUserToDemote);

    }

    @Test
    public void update_should_throwException_whenUser_IsNot_UpdatingHimself_andIsNot_Admin() {
        //Arrange
        User user = createMockUser();
        User user2 = createMockUser();
        user2.setId(4);


        //Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> userService.update(user2, user));


    }

    @Test
    public void update_should_throwException_when_newUsername_isDuplicate() {
        User updatedUser = createMockUser();

        User existingUser = createMockUser();
        existingUser.setId(4);

        when(mockUserRepository.getUserByUsername(Mockito.anyString())).thenReturn(existingUser);

        //Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> userService.update(updatedUser, updatedUser));


    }

    @Test
    public void update_should_throwException_when_newEmail_isDuplicate() {
        User updatedUser = createMockUser();

        User existingUser = createMockUser();
        existingUser.setId(4);

        when(mockUserRepository.getUserByUsername(Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        when(mockUserRepository.getUserByEmail(updatedUser.getEmail())).thenReturn(existingUser);


        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> userService.update(updatedUser, updatedUser));
    }

    @Test
    public void update_should_updateUser_when_loggedUser_IsAdmin_thereIsNoDuplication() {
        User loggedUser = createMockAdmin();
        User updatedUser = createMockUser();

        when(mockUserRepository.getUserByUsername(Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        when(mockUserRepository.getUserByEmail(Mockito.anyString())).thenThrow(EntityNotFoundException.class);

        userService.update(updatedUser,loggedUser);

        Mockito.verify(mockUserRepository,Mockito.times(1)).update(updatedUser);
    }

    @Test
    public void update_should_updateUser_when_userUpdatesHimself_and_thereIsNoDuplication() {
        User loggedUser = createMockAdmin();

        when(mockUserRepository.getUserByUsername(Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        when(mockUserRepository.getUserByEmail(Mockito.anyString())).thenThrow(EntityNotFoundException.class);

        userService.update(loggedUser,loggedUser);

        Mockito.verify(mockUserRepository,Mockito.times(1)).update(loggedUser);
    }

    @Test
    public void delete_should_throwException_whenUser_IsNot_DeletingHimself_andIsNot_Admin() {
        //Arrange
        User user = createMockUser();
        User user2 = createMockUser();
        user2.setId(4);

        when(mockUserRepository.getUserById(Mockito.anyInt())).thenReturn(user2);

        //Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> userService.delete(user2.getId(), user));


    }

    @Test
    public void delete_should_deleteUser_when_loggedUser_IsAdmin() {
        User loggedUser = createMockAdmin();
        User userToDelete = createMockUser();

        when(mockUserRepository.getUserById(Mockito.anyInt())).thenReturn(userToDelete);

        userService.delete(userToDelete.getId(),loggedUser);

        Mockito.verify(mockUserRepository,Mockito.times(1)).delete(userToDelete);
    }

    @Test
    public void delete_should_deleteUser_whenUser_DeletesHimself() {
        User loggedUser = createMockAdmin();
        when(mockUserRepository.getUserById(Mockito.anyInt())).thenReturn(loggedUser);

        userService.delete(loggedUser.getId(),loggedUser);

        Mockito.verify(mockUserRepository,Mockito.times(1)).delete(loggedUser);
    }


}