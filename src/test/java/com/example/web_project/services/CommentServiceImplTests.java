package com.example.web_project.services;

import com.example.web_project.models.*;
import com.example.web_project.repositories.*;
import com.example.web_project.utils.exceptions.BlockedUserException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.web_project.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CommentServiceImplTests {

    @Mock
    private CommentRepository mockCommentRepository;
    @Mock
    private VoteRepository mockVoteRepository;
    @InjectMocks
    private CommentServiceImpl mockCommentService;

    @Test
    public void getAll_Should_CallRepository() {
        // Arrange
        when(mockCommentRepository.getAll(any(), any(), any(), any()))
                .thenReturn(null);

        // Act
        mockCommentService.getAll(any(), any(), any(), any());

        // Assert
        verify(mockCommentRepository, times(1)).getAll(any(), any(), any(), any());
    }

    @Test
    public void getCommentById_Should_ReturnComment_When_CommentWithIdExist() {
        // Arrange
        Comment mockComment = createMockComment();

        when(mockCommentRepository.getCommentById(anyInt()))
                .thenReturn(mockComment);

        // Act
        Comment mockResult = mockCommentService.getCommentById(mockComment.getId());

        // Assert
        assertEquals(mockComment, mockResult);
    }

    @Test
    public void create_Should_ThrowException_When_UserIsBlocked() {
        // Arrange
        User mockUser = createMockUser();
        mockUser.setBlocked(true);

        Comment mockComment = createMockComment();
        mockComment.setCreator(mockUser);

        // Act, Assert
        assertThrows(
                BlockedUserException.class,
                () -> mockCommentService.create(mockComment));
    }

    @Test
    public void create_Should_CallRepository_When_UserIsNotBlocked() {
        // Arrange
        Comment mockComment = createMockComment();
        mockComment.setCreator(createMockUser());

        // Act
        mockCommentService.create(mockComment);

        // Assert
        verify(mockCommentRepository, Mockito.times(1)).create(mockComment);
    }

    @Test
    public void update_Should_ThrowException_When_CreatorIsBlocked() {
        // Arrange
        User mockUser = createMockUser();
        mockUser.setBlocked(true);

        Comment mockComment = createMockComment();
        mockComment.setCreator(mockUser);

        // Act, Assert
        assertThrows(
                BlockedUserException.class,
                () -> mockCommentService.update(mockComment, mockUser));
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotCreatorOrAdmin() {
        // Arrange
        User mockUser = createMockUser();

        Comment mockComment = createMockComment();
        mockComment.setCreator(mockUser);

        when(mockCommentRepository.getCommentById(Mockito.anyInt()))
                .thenReturn(mockComment);

        // Act, Assert
        assertThrows(
                UnauthorizedOperationException.class,
                () -> mockCommentService.update(mockComment, Mockito.mock(User.class)));
    }

    @Test
    public void update_Should_CallRepository_When_UserIsCreator() {
        // Arrange
        User mockUser = createMockUser();

        Comment mockComment = createMockComment();
        mockComment.setCreator(mockUser);

        when(mockCommentRepository.getCommentById(anyInt()))
                .thenReturn(mockComment);

        // Act
        mockCommentService.update(mockComment, mockUser);

        // Assert
        verify(mockCommentRepository, times(1))
                .update(mockComment);
    }

    @Test
    public void update_Should_CallRepository_When_UserIsAdmin() {
        // Arrange
        User mockUserAdmin = createMockAdmin();

        Comment mockComment = createMockComment();
        mockComment.setCreator(mock(User.class));

        when(mockCommentRepository.getCommentById(Mockito.anyInt()))
                .thenReturn(mockComment);

        // Act
        mockCommentService.update(mockComment, mockUserAdmin);

        // Assert
        verify(mockCommentRepository, times(1))
                .update(mockComment);
    }

    @Test
    public void delete_Should_ThrowException_When_UserIsNotCreatorOrAdmin() {
        // Arrange
        User mockUser = createMockUser();

        Comment mockComment = createMockComment();
        mockComment.setCreator(mockUser);

        when(mockCommentRepository.getCommentById(Mockito.anyInt()))
                .thenReturn(mockComment);

        // Act, Assert
        assertThrows(
                UnauthorizedOperationException.class,
                () -> mockCommentService.delete(mockComment.getId(), Mockito.mock(User.class)));
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsCreator() {
        // Arrange
        User mockUser = createMockUser();

        Comment mockComment = createMockComment();
        mockComment.setCreator(mockUser);

        when(mockCommentRepository.getCommentById(Mockito.anyInt()))
                .thenReturn(mockComment);
        // Act
        mockCommentService.delete(mockComment.getId(), mockUser);
        // Assert
        verify(mockCommentRepository, Mockito.times(1))
                .delete(mockComment.getId());
    }

    @Test
    public void delete_Should_CallRepository_When_UserIsAdmin() {
        // Arrange
        User mockUserAdmin = createMockAdmin();

        Comment mockComment = createMockComment();
        mockComment.setCreator(new User());

        when(mockCommentRepository.getCommentById(Mockito.anyInt()))
                .thenReturn(mockComment);

        // Act
        mockCommentService.delete(mockComment.getId(), mockUserAdmin);

        // Assert
        verify(mockCommentRepository, times(1))
                .delete(mockComment.getId());
    }

    @Test
    public void addVote_Should_AddVoteToVotes() {
        // Arrange
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();
        Vote mockVote = createLike();
        when(mockVoteRepository.getById(anyInt()))
                .thenReturn(mockVote);

        // Act
        mockCommentService.addVote(mockComment, mockUser, anyInt());

        // Assert
        assertTrue(mockComment.getVotes().containsKey(mockUser));
        assertTrue(mockComment.getVotes().containsValue(mockVote));
        verify(mockCommentRepository, times(1))
                .update(mockComment);
    }

    @Test
    public void removeVote_should_removeVoteFromVotes() {
        //Arrange
        User mockUser = createMockUser();
        Comment mockComment = createMockComment();

        //Act
        mockCommentService.removeVote(mockComment, mockUser);

        //Assert
        assertFalse(mockComment.getVotes().containsKey(mockUser));
        verify(mockCommentRepository, times(1))
                .update(mockComment);
    }
}