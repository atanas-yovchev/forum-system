package com.example.web_project.services;

import com.example.web_project.models.Phone;
import com.example.web_project.models.User;
import com.example.web_project.repositories.PhoneRepository;
import com.example.web_project.repositories.PhoneRepositoryImpl;
import com.example.web_project.utils.exceptions.DuplicateEntityException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static com.example.web_project.Helpers.*;
import static org.mockito.Mockito.when;

public class PhoneServiceImplTests {

    private PhoneRepository phoneRepository;

    private PhoneServiceImpl phoneService;

    @BeforeEach
    public void initDependencies() {
        this.phoneRepository = Mockito.mock(PhoneRepositoryImpl.class);
        this.phoneService = new PhoneServiceImpl(phoneRepository);
    }


    @Test
    public void create_should_throwException_whenLoggedUser_isNot_EditingHimself() {
        User user = createMockAdmin();
        User user2 = createMockAdmin();
        user2.setId(5);
        Phone phone = createMockPhone(user2);

        Assertions.assertThrows(UnauthorizedOperationException.class,() -> phoneService.create(phone,user));
    }

    @Test
    public void create_should_throwException_whenLoggedUser_isNot_Admin() {
        User user = createMockUser();
        Phone phone = createMockPhone(user);

        Assertions.assertThrows(UnauthorizedOperationException.class,() -> phoneService.create(phone,user));
    }

    @Test
    public void create_should_throwException_whenUser_hasPhone() {
        User user = createMockAdmin();
        Phone existingPhone = createMockPhone(user);
        Phone newPhone = createMockPhone(user);
        when(phoneRepository.getPhoneByNumber(Mockito.anyString())).thenThrow(EntityNotFoundException.class);
        //To make the user have no phone, change the line below to throw EntityNotFoundException.
        when(phoneRepository.getPhoneByUser(user)).thenReturn(existingPhone);


        Assertions.assertThrows(DuplicateEntityException.class,() -> phoneService.create(newPhone,user));
    }

    @Test
    public void create_should_throwException_when_phoneExists() {
        User user = createMockAdmin();
        Phone phone = createMockPhone(user);
        when(phoneRepository.getPhoneByUser(user)).thenThrow(EntityNotFoundException.class);
        //To make the phone unique, change the line below to throw EntityNotFoundException.
        when(phoneRepository.getPhoneByNumber(Mockito.anyString())).thenReturn(phone);


        Assertions.assertThrows(DuplicateEntityException.class,() -> phoneService.create(phone,user));
    }

    @Test
    public void create_should_createPhone_when_validData() {
        User user = createMockAdmin();
        Phone phone = createMockPhone(user);
        when(phoneRepository.getPhoneByUser(user)).thenThrow(EntityNotFoundException.class);
        when(phoneRepository.getPhoneByNumber(Mockito.anyString())).thenThrow(EntityNotFoundException.class);

        phoneService.create(phone,user);

        Mockito.verify(phoneRepository,Mockito.times(1)).create(phone);
    }


    @Test
    public void update_should_throwException_whenLoggedUser_isNot_EditingHimself() {
        User user = createMockAdmin();
        User user2 = createMockAdmin();
        user2.setId(5);
        Phone phone = createMockPhone(user2);

        Assertions.assertThrows(UnauthorizedOperationException.class,() -> phoneService.update(phone,user));
    }

    @Test
    public void update_should_throwException_whenLoggedUser_isNot_Admin() {
        User user = createMockUser();
        Phone phone = createMockPhone(user);

        Assertions.assertThrows(UnauthorizedOperationException.class,() -> phoneService.update(phone,user));
    }

    @Test
    public void update_should_throwException_when_phoneExists() {
        User user = createMockAdmin();
        Phone phone = createMockPhone(user);
        when(phoneRepository.getPhoneByUser(user)).thenThrow(EntityNotFoundException.class);
        //To make the phone unique, change the line below to throw EntityNotFoundException.
        when(phoneRepository.getPhoneByNumber(Mockito.anyString())).thenReturn(phone);


        Assertions.assertThrows(DuplicateEntityException.class,() -> phoneService.update(phone,user));
    }

    @Test
    public void update_should_updatePhone_when_validData() {
        User user = createMockAdmin();
        Phone phone = createMockPhone(user);
        when(phoneRepository.getPhoneByUser(user)).thenThrow(EntityNotFoundException.class);
        when(phoneRepository.getPhoneByNumber(Mockito.anyString())).thenThrow(EntityNotFoundException.class);

        phoneService.update(phone,user);

        Mockito.verify(phoneRepository,Mockito.times(1)).update(phone);
    }

    @Test
    public void delete_should_throwException_whenLoggedUser_isNot_EditingHimself() {
        User user = createMockAdmin();
        User user2 = createMockAdmin();
        user2.setId(5);
        Phone phone = createMockPhone(user2);

        Assertions.assertThrows(UnauthorizedOperationException.class,() -> phoneService.delete(phone,user));
    }

    @Test
    public void delete_should_throwException_whenLoggedUser_isNot_Admin() {
        User user = createMockUser();
        Phone phone = createMockPhone(user);

        Assertions.assertThrows(UnauthorizedOperationException.class,() -> phoneService.delete(phone,user));
    }

    @Test
    public void delete_should_deletePhone_when_validData() {
        User user = createMockAdmin();
        Phone phone = createMockPhone(user);

        phoneService.delete(phone,user);

        Mockito.verify(phoneRepository,Mockito.times(1)).delete(phone);
    }

    @Test
    public void getPhoneByUser_should_throwException_whenLoggedUser_isNot_EditingHimself() {
        User user = createMockAdmin();
        User user2 = createMockAdmin();
        user2.setId(5);

        Assertions.assertThrows(UnauthorizedOperationException.class,() -> phoneService.getPhoneByUser(user2,user));
    }

    @Test
    public void getPhoneByUser_should_throwException_whenLoggedUser_isNot_Admin() {
        User user = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,() -> phoneService.getPhoneByUser(user,user));
    }

    @Test
    public void getPhoneByUser_should_deletePhone_when_validData() {
        User user = createMockAdmin();

        phoneService.getPhoneByUser(user, user);

        Mockito.verify(phoneRepository,Mockito.times(1)).getPhoneByUser(user);
    }
}
