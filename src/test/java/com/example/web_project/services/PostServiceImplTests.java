package com.example.web_project.services;

import com.example.web_project.models.Post;
import com.example.web_project.models.Tag;
import com.example.web_project.models.User;
import com.example.web_project.models.Vote;
import com.example.web_project.repositories.PostRepository;
import com.example.web_project.repositories.TagRepository;
import com.example.web_project.repositories.VoteRepository;
import com.example.web_project.utils.exceptions.BlockedUserException;
import com.example.web_project.utils.exceptions.EntityNotFoundException;
import com.example.web_project.utils.exceptions.UnauthorizedOperationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.List;

import static com.example.web_project.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PostServiceImplTests {
    @Mock
    private PostRepository postRepository;
    @Mock
    private TagRepository tagRepository;
    @Mock
    private VoteRepository voteRepository;

    @Mock
    private CommentService commentService;
    @InjectMocks
    private PostServiceImpl postService;

    @Test
    public void get_should_callRepository() {
        //Arrange
        when(postRepository.getAll(Mockito.any()))
                .thenReturn(null);
        //Act
        postService.getAll(Mockito.any());

        //Assert
        verify(postRepository, times(1))
                .getAll(Mockito.any());

    }

    @Test
    public void get_should_returnPost_when_postWhitIdExist() {
        //Arrange
        Post mockPost = createMockPost();
        when(postRepository.getPostById(Mockito.anyInt()))
                .thenReturn(mockPost);
        //Act
        Post result = postService.getPostById(mockPost.getId());

        //Assert
        Assertions.assertEquals(mockPost, result);
    }

    @Test
    public void create_should_createPost_when_creatorIsNotBlocked() {
        //Arrange
        User mockUser = createMockUser();
        mockUser.setBlocked(false);
        Post post = createMockPost(mockUser);
        when(postRepository.create(post)).thenReturn(null);

        //Act
        postService.create(post);

        //Assert
        verify(postRepository, times(1))
                .create(post);
    }

    @Test
    public void create_should_throwsException_when_creatorIsBlocked() {
        //Arrange
        User mockUser = createMockUser();
        mockUser.setBlocked(true);
        Post mockPost = createMockPost(mockUser);

        //Act,Assert
        assertThrows(BlockedUserException.class,
                () -> postService.create(mockPost));
    }

    @Test
    public void update_should_updatePost_when_creatorIsNotBlocked() {
        //Arrange
        User mockUser = createMockUser();
        mockUser.setBlocked(false);
        Post mockPost = createMockPost(mockUser);
        when(postRepository.update(mockPost)).thenReturn(null);

        //Act
        postService.update(mockPost, mockUser);

        //Assert
        verify(postRepository, times(1))
                .update(mockPost);

    }

    @Test
    public void update_should_throwsException_when_creatorIsBlocked() {
        //Arrange
        User mockUser = createMockUser();
        mockUser.setBlocked(true);
        Post mockPost = createMockPost(mockUser);

        //Act,Assert
        assertThrows(BlockedUserException.class,
                () -> postService.update(mockPost, mockUser));
    }

    @Test
    public void update_should_throwsException_when_userNotHavePermissions() {
        //Arrange
        User creator = createMockUser();
        User mockUser = new User();
        mockUser.setId(2);
        mockUser.setRoles(new HashSet<>());
        Post mockPost = createMockPost(creator);

        //Act,Assert
        assertThrows(UnauthorizedOperationException.class,
                () -> postService.update(mockPost, mockUser));
    }

    @Test
    public void delete_should_deletePost_when_userHavePermissions() {
        //Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost(mockUser);
        mockPost.setComments(new HashSet<>());
        when(postRepository.getPostById(anyInt())).thenReturn(mockPost);
        when(postRepository.delete(mockPost)).thenReturn(null);

        //Act
        postService.delete(mockPost.getId(), mockUser);

        //Assert
        verify(postRepository, times(1)).delete(mockPost);
    }

    @Test
    public void delete_should_throwsException_when_userNotHavePermissions() {
        //Arrange
        User creator = createMockUser();
        User mockUser = new User();
        mockUser.setId(2);
        mockUser.setRoles(new HashSet<>());
        Post mockPost = createMockPost(creator);
        when(postRepository.getPostById(Mockito.anyInt())).thenReturn(mockPost);


        //Act,Assert
        assertThrows(UnauthorizedOperationException.class,
                () -> postService.delete(mockPost.getId(), mockUser));
    }

    @Test
    public void addVote_should_addVoteToVotes() {
        //Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost();
        Vote vote = createLike();
        when(voteRepository.getById(anyInt())).thenReturn(vote);

        //Act
        postService.addVote(mockUser, mockPost, anyInt());

        //Assert
        assertTrue(mockPost.getVotes().containsKey(mockUser));
        assertTrue(mockPost.getVotes().containsValue(vote));
        verify(postRepository, times(1)).update(mockPost);
    }

    @Test
    public void removeVote_should_removeVoteFromVotes() {
        //Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost();


        //Act
        postService.removeVote(mockUser, mockPost);

        //Assert
        assertFalse(mockPost.getVotes().containsKey(mockUser));
        verify(postRepository, times(1)).update(mockPost);
    }

    @Test
    public void addTags_should_addTagsToPost_when_userHavePermissions() {
        //Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost(mockUser);
        Tag mockTag = createMockTag();
        when(tagRepository.getByName(anyString())).thenReturn(mockTag);

        //Act
        postService.addTags(mockUser, mockPost, List.of(mockTag));

        //Assert
        assertTrue(mockPost.getTags().contains(mockTag));
        verify(postRepository, times(1)).update(mockPost);
    }

    @Test
    public void addTags_should_throwsException_when_userNotHavePermissions() {
        //Arrange
        User mockUser = createMockUser();
        mockUser.setRoles(new HashSet<>());
        User creator = new User();
        creator.setId(2);
        Post mockPost = createMockPost(creator);
        Tag mockTag = createMockTag();

        //Act,Assert
        assertThrows(UnauthorizedOperationException.class,
                () -> postService.addTags(mockUser, mockPost, List.of(mockTag)));

    }

    @Test
    public void addTags_should_createTag_when_tagNotExist() {
        //Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost(mockUser);
        Tag mockTag = createMockTag();
        when(tagRepository.getByName(anyString())).thenThrow(EntityNotFoundException.class);

        //Act
        postService.addTags(mockUser, mockPost, List.of(mockTag));

        //Assert
        verify(tagRepository, times(1)).create(mockTag);
    }

    @Test
    public void removeTag_should_removeTagFromPost_when_tagExistOnPost() {
        //Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost(mockUser);
        Tag tag = createMockTag();
        mockPost.addTags(tag);
        when(tagRepository.getById(anyInt())).thenReturn(tag);

        //Act
        postService.removeTag(mockUser, mockPost, anyInt());

        //Assert
        assertFalse(mockPost.getTags().contains(tag));
        verify(postRepository, times(1)).update(mockPost);
    }

    @Test
    public void removeTag_should_throwsException_when_tagNotExistOnPost() {
        //Arrange
        User mockUser = createMockUser();
        Post mockPost = createMockPost(mockUser);
        Tag tag = createMockTag();
        when(tagRepository.getById(anyInt())).thenReturn(tag);

        //Act,Assert
        assertThrows(EntityNotFoundException.class,
                () -> postService.removeTag(mockUser, mockPost, anyInt()));
    }

    @Test
    public void removeTag_should_throwsException_when_userNotHavePermissions() {
        //Arrange
        User mockUser = createMockUser();
        mockUser.setRoles(new HashSet<>());
        User creator = new User();
        creator.setId(2);
        Post mockPost = createMockPost(creator);
        Tag tag = createMockTag();

        //Act,Assert
        assertThrows(UnauthorizedOperationException.class,
                () -> postService.removeTag(mockUser, mockPost, tag.getId()));

    }

    @Test
    public void deleteTag_should_deleteTag_when_userIsAdmin() {
        //Arrange
        User mockUser = createMockAdmin();
        Post mockPost = createMockPost();
        Tag mockTag = createMockTag();
        mockPost.addTags(mockTag);
        when(tagRepository.getById(anyInt())).thenReturn(mockTag);
        when(postRepository.getAllByTagName(anyString())).thenReturn(List.of(mockPost));

        //Act
        postService.deleteTag(mockUser, anyInt());

        //Assert
        verify(postRepository, times(1)).getAllByTagName(mockTag.getName());
        assertFalse(mockPost.getTags().contains(mockTag));
        verify(tagRepository, times(1)).delete(mockTag);
    }

    @Test
    public void deleteTag_should_throwsException_when_userIsNotAdmin() {
        //Arrange
        User mockUser = createMockUser();
        Tag mockTag = createMockTag();

        //Act,Assert
        assertThrows(UnauthorizedOperationException.class,
                () -> postService.deleteTag(mockUser, mockTag.getId()));
    }
}
