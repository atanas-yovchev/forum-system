package com.example.web_project;


import com.example.web_project.models.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;

public class Helpers {

    //Phone should be added manually
    public static User createMockAdmin() {
        User admin = createMockUser();
        admin.setId(99);
        admin.addRole(createAdminRole());
        createMockPhone(admin);
        return admin;
    }

    public static Phone createMockPhone(User user) {
        Phone phone = new Phone();
        phone.setUser(user);
        phone.setPhoneNumber("+359864241255");
        return phone;
    }

    public static Role createAdminRole() {
        Role adminRole = new Role();
        adminRole.setName("admin");
        adminRole.setId(1);
        return adminRole;
    }

    public static User createMockUser() {
        User user = new User();
        user.setId(1);
        user.setEmail("validemail@emails.com");
        user.setUsername("testuser1");
        user.setCreatedDate(LocalDateTime.now());
        user.setFirstName("Testomir");
        user.setLastName("Userov");
        user.setPassword("parolkaa");
        user.setRoles(new HashSet<>());
        return user;
    }

    public static Post createMockPost() {
        return createMockPost(createMockUser());
    }

    public static Post createMockPost(User creator) {
        Post post = new Post();
        post.setId(1);
        post.setCreator(creator);
        post.setTitle("Valid Test Title");
        post.setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut " +
                "labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi " +
                "ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum " +
                "dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
                "deserunt mollit anim id est laborum.");
        post.setTags(new HashSet<>());
        post.setVotes(new HashMap<>());
        post.setCreationDate(LocalDateTime.now());

        return post;
    }

    public static Comment createMockComment() {
        return  createMockComment(createMockPost(createMockUser()), createMockUser());
    }

    public static Comment createMockComment(Post post, User creator) {
        Comment comment = new Comment();
        comment.setId(1);
        comment.setPost(post);
        comment.setCreator(creator);
        comment.setContent("Duis aute irure dolor in reprehenderit in voluptate velit esse cillum " +
                "dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
                "deserunt mollit anim id est laborum.");
        comment.setVotes(new HashMap<>());
        comment.setCreationDate(LocalDateTime.now());

        return comment;
    }

    public static Tag createMockTag() {
        Tag tag = new Tag();
        tag.setName("mockTag");
        tag.setId(1);
        return tag;
    }

    public static Vote createLike() {
        Vote vote = new Vote();
        vote.setId(1);
        vote.setVoteType("like");
        return vote;
    }

    public static Vote createDislike() {
        Vote vote = new Vote();
        vote.setId(2);
        vote.setVoteType("dislike");
        return vote;
    }



    /**
     * Accepts an object and returns the stringified object.
     * Useful when you need to pass a body to a HTTP request.
     */
    public static String toJson(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
