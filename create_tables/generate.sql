create database if not exists `forum_system`;
use `forum_system`;


create table roles
(
    role_id   int auto_increment
        primary key,
    role_name varchar(20) not null,
    constraint roles_pk
        unique (role_name)
);

create table tags
(
    tag_id   int auto_increment
        primary key,
    tag_name varchar(30) null,
    constraint tags_pk
        unique (tag_name)
);

create table users
(
    user_id     int auto_increment
        primary key,
    first_name  varchar(32)  not null,
    last_name   varchar(32)  not null,
    email       varchar(60)  not null,
    username    varchar(32)  not null,
    password    varchar(32)  not null,
    created_at  datetime     not null,
    avatar_path varchar(128) default '/assets/avatars/default/OIP.jpeg' not null,
    is_blocked  tinyint(1)   not null,
    is_deleted  tinyint(1)   not null,
    constraint users_email_uk
        unique (email),
    constraint users_username_uk
        unique (username)
);

create table phone_numbers
(
    phone_number varchar(15) not null,
    user_id      int         not null
        primary key,
    constraint phone_numbers_uk
        unique (phone_number),
    constraint phone_numbers_users_null_fk
        foreign key (user_id) references users (user_id)
);

create table posts
(
    post_id    int auto_increment
        primary key,
    user_id    int         not null,
    title      varchar(64) not null,
    content    text        not null,
    created_at datetime    not null,
    constraint posts_users_null_fk
        foreign key (user_id) references users (user_id)
);

create table comments
(
    comment_id int auto_increment
        primary key,
    post_id    int  not null,
    user_id    int  not null,
    content    text not null,
    created_at date not null,
    constraint comments_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint created_by_fk
        foreign key (user_id) references users (user_id)
);

create table posts_tags
(
    post_id int not null,
    tag_id  int not null,
    constraint posts_tags_post_fk
        foreign key (post_id) references posts (post_id),
    constraint posts_tags_tag_fk
        foreign key (tag_id) references tags (tag_id)
);

create table users_roles
(
    user_id int not null,
    role_id int not null,
    constraint users_roles_role_fk
        foreign key (role_id) references roles (role_id),
    constraint users_roles_users_null_fk
        foreign key (user_id) references users (user_id)
);

create table votes
(
    vote_id   int         not null
        primary key,
    vote_type varchar(50) not null
);

create table comments_votes
(
    comment_id int null,
    user_id    int null,
    vote_id    int null,
    constraint comments_votes_comments_null_fk
        foreign key (comment_id) references comments (comment_id),
    constraint comments_votes_users_null_fk
        foreign key (user_id) references users (user_id),
    constraint comments_votes_votes_null_fk
        foreign key (vote_id) references votes (vote_id)
);

create table posts_votes
(
    post_id int null,
    user_id int null,
    vote_id int null,
    constraint posts_votes_posts_null_fk
        foreign key (post_id) references posts (post_id),
    constraint posts_votes_users_null_fk
        foreign key (user_id) references users (user_id),
    constraint posts_votes_votes_null_fk
        foreign key (vote_id) references votes (vote_id)
);
